Para compilar, é necessário ter algum JDK (e.g. OpenJDK, Oracle JDK) instalado, além do Maven.

Para executar, basta rodar o `run.sh` passando o modo de execução. Se o projeto não estiver compilado ainda, o `run.sh` irá compilá-lo automaticamente. Algumas variáveis de ambiente podem ser passadas para alterar o comportamento do script e a execução do programa.

Por exemplo, para executar servidor e cliente:

```
$ ./run.sh servidor &
$ ./run.sh cliente
```

