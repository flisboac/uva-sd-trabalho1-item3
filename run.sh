#!/bin/sh

[ -z "$JAVA"        ] && JAVA=java
[ -z "$MAVEN"       ] && MAVEN=mvn
[ -z "$JAR"         ] && JAR=target/uvafat.jar
[ -z "$RMIREGISTRY" ] && RMIREGISTRY=rmiregistry
[ -z "$PID_FILE"    ] && PID_FILE="rmiregistry.pid"

if [ ! -f "$JAR" ]; then
    "$MAVEN" package
fi

modo="local"; [ ! -z "$1" ] && modo=$1

case "$modo" in
local)
    "$JAVA" -jar "$JAR" local
    ;;
cliente)
    if [ ! -f "$PID_FILE" ]; then
        echo "ERRO: Servidor não está em execução! Execute '$0 servidor' antes!"
        exit 1;
    else
        "$JAVA" -jar "$JAR" cliente
    fi
    ;;
servidor)
    # -JDjava.security.policy=java.policy -JDjava.security.debug=access,failure
    export CLASSPATH="$JAR"
    "$RMIREGISTRY" -J-Djava.rmi.server.hostname=127.0.0.1 </dev/null &>/dev/null &
    RMIREGISTRY_PID=$!
    echo "$RMIREGISTRY_PID" > "$PID_FILE"

    # -Djava.security.policy=java.policy -Djava.security.debug=access,failure
    "$JAVA" "-Djava.rmi.server.hostname=127.0.0.1" -jar "$JAR" servidor
    rm -f "$PID_FILE"
    kill -15 "$RMIREGISTRY_PID"
    ;;
*)
    echo "Uso: $0 <local|cliente|servidor>"
    ;;
esac

