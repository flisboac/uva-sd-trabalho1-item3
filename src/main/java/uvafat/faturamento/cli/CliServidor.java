package uvafat.faturamento.cli;

import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.*;

public class CliServidor extends CliBase implements ServicoFaturamentoRmi.Observador, Serializable {

    private ServicoFaturamentoRmi instancia;
    private Object idObservador;
    private final List<Long> idsTransacao = new ArrayList<>();

    public CliServidor(ServicoFaturamentoRmi instancia) {
        this.instancia = instancia;
    }

    @Override
    public void executar() throws RemoteException {
        idObservador = instancia.observar(this);

        imprimirMensagem("* Servidor iniciado. Pressione 'A' para aprovar a próxima transação da lista, ou 'R' para reprová-la. Pressione 'Q' para cancelar.");
        Optional<Character> opcao = Optional.empty();

        while (!opcao.isPresent() || !opcao.get().equals('C')) {
            opcao = lerCaractere("[aArRqQ]");
            switch (opcao.get()) {
            case 'A':
                finalizarTransacao(true);
                break;
            case 'R':
                finalizarTransacao(false);
                break;
            case 'Q':
                imprimirMensagem("* Finalizando Servidor...");
                break;
            }
        }

        instancia.removerObservador(idObservador);
    }

    private void finalizarTransacao(boolean aprovada) {
        Long idTransacao = !idsTransacao.isEmpty() ? idsTransacao.remove(0) : null;
        String operacao = aprovada ? "aprovar" : "cancelar";
        if (idTransacao != null) {
            imprimirMensagem("[INFO] Tentando %s transação com ID %d...", operacao, idTransacao);
        } else {
            imprimirMensagem("[ERRO] Não há transações para %s.", operacao);
        }
    }

    private Optional<Character> lerCaractere(String opcoes) {
        try {
            return Optional.of(inScanner.next(opcoes).charAt(0));
        } catch (NoSuchElementException e) {
            return Optional.empty();
        }
    }
    @Override
    public void notificar(ServicoFaturamentoRmi.Evento evento) {
        switch(evento.getTipo()) {
        case TRANSACAO_ADICIONADA:
            imprimirMensagem("* [INFO] Transação (ID:%d) adicionada.", evento.getIdEntidade());
            idsTransacao.add(evento.getIdEntidade());
            break;

        case USUARIO_ADICIONADO:
            imprimirMensagem("* [INFO] Usuário (ID:%d) adicionado.", evento.getIdEntidade());
            break;

        case MEIO_PAGAMENTO_ADICIONADO:
            imprimirMensagem("* [INFO] Meio de pagamento (ID:%d) adicionado.", evento.getIdEntidade());
            break;
        }
    }
}
