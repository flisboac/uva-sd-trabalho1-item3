package uvafat.faturamento.cli.provedores;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmiClient;

public interface ProvedorServicosCli extends AutoCloseable {

    ServicoFaturamento instanciar();
    ServicoFaturamentoRmiClient conectar(ServicoFaturamentoRmi.OpcoesConexao opcoes);
    ServicoFaturamentoRmi iniciar(ServicoFaturamentoRmi.OpcoesServico opcoes);
    void close();
}

