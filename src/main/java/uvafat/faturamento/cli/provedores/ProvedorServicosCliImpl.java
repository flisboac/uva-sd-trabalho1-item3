package uvafat.faturamento.cli.provedores;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;
import uvafat.faturamento.aplicacao.impl.local.ServicoFaturamentoLocal;
import uvafat.faturamento.aplicacao.impl.rmi.ServicoFaturamentoRmiClientImpl;
import uvafat.faturamento.aplicacao.impl.rmi.ServicoFaturamentoRmiImpl;

public class ProvedorServicosCliImpl implements ProvedorServicosCli {


    @Override
    public ServicoFaturamento instanciar() {
        return ServicoFaturamentoLocal.instanciar();
    }

    @Override
    public ServicoFaturamentoRmiClientImpl conectar(ServicoFaturamentoRmi.OpcoesConexao opcoes) {
        return ServicoFaturamentoRmiClientImpl.conectar(opcoes);
    }

    @Override
    public ServicoFaturamentoRmi iniciar(ServicoFaturamentoRmi.OpcoesServico opcoes) {
        return ServicoFaturamentoRmiImpl.iniciar(opcoes);
    }

    @Override
    public void close() {
    }
}
