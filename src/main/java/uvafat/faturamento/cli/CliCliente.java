package uvafat.faturamento.cli;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemMeioPagamento;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemTransacao;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemUsuario;
import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;

import java.io.InputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class CliCliente extends CliBase implements Runnable {

    private enum Operacao {
        INVALIDA(0, "Operação inválida. Tente novamente."),
        CADASTRAR_USUARIO(1, "Cadastrar novo usuário"),
        BUSCAR_USUARIO(2, "Mostrar dados de usuário"),
        CADASTRAR_MEIO_PAGAMENTO(3, "Cadastrar novo meio de pagamento"),
        INICIAR_TRANSACAO(4, "Iniciar transação"),
        SAIR(9, "Sair");

        public final int id;
        public final String nome;

        Operacao(int id, String nome) {
            this.id = id;
            this.nome = nome;
        }

        public static List<Operacao> getOpcoes() {
            return Arrays.asList(values()).stream()
                .filter(operacao -> operacao != INVALIDA)
                .collect(Collectors.toList());
        }

        public static Operacao buscarPorId(Integer id) {
            if (id != null) for (Operacao operacao : values()) if (operacao.id == id) return operacao;
            return INVALIDA;
        }

        public static Operacao buscarPorId(Optional<Integer> id) {
            return buscarPorId(id.orElse(null));
        }
    }

    private ServicoFaturamento servico;

    public CliCliente(ServicoFaturamento servico) {
        this.servico = servico;
    }

    protected void executar() {
        Operacao opcao = Operacao.INVALIDA;

        imprimirMensagem();
        while (opcao != Operacao.SAIR) {
            imprimirMensagem("-=[[ SISTEMA DE FATURAMENTO ]]=-");
            imprimirMensagem("MENU PRINCIPAL");
            for (Operacao operacao : Operacao.getOpcoes()) imprimirOpcao(operacao);

            imprimirMensagem("Digite uma opção: ");
            Optional<Integer> idOpcao = lerInt();
            opcao = Operacao.buscarPorId(idOpcao);

            switch (opcao) {
            case CADASTRAR_USUARIO:
                cadastrarUsuario();
                break;

            case BUSCAR_USUARIO:
                buscarUsuario();
                break;

            case CADASTRAR_MEIO_PAGAMENTO:
                cadastrarMeioPagamento();
                break;

            case INICIAR_TRANSACAO:
                iniciarTransacao();
                break;

            case INVALIDA:
                imprimirMensagem(opcao.nome);
                break;

            case SAIR:
                break;
            }
        }
    }

    private void cadastrarUsuario() {
        RequisicaoCriarUsuario requisicao = new RequisicaoCriarUsuario();
        String nomeUsuario;

        // Obter dados
        imprimirMensagem("Digite o nome do novo usuário: ");
        nomeUsuario = lerLinha();

        // Preparar e executar chamada
        requisicao.setNome(nomeUsuario);
        RespostaCriarUsuario resposta = servico.criarUsuario(requisicao);

        if (resposta.isSucesso()) {
            imprimirMensagem("Usuário (ID:%d) criado com sucesso.", resposta.getIdUsuario());

        } else {
            imprimirErroRequisicao("Um erro ocorreu ao tentar criar o usuário.", resposta);
        }
    }

    private void buscarUsuario() {
        Optional<MensagemUsuario> optUsuario = lerUsuario();
        if (!optUsuario.isPresent()) return;
        MensagemUsuario usuario = optUsuario.get();

        // Buscar transações e meios de pagamento
        Optional<List<MensagemMeioPagamento>> meiosPagamento = lerMeiosPagamento("   - ", usuario.getId());

        RequisicaoListarTransacoes requisicaoTransacoes = new RequisicaoListarTransacoes();
        requisicaoTransacoes.setIdUsuario(usuario.getId());
        RespostaListarTransacoes respostaTransacoes = servico.listarTransacoes(requisicaoTransacoes);

        // Imprimir dados
        imprimirMensagem("Usuário '%s' (ID: %d)", usuario.getNome(), usuario.getId());

        imprimirMensagem(" - Meios de Pagamento (Total: %d)", usuario.getQtdMeiosPagamento());
        if (meiosPagamento.isPresent()) {
            for (MensagemMeioPagamento meioPagamento : meiosPagamento.get()) {
                imprimirMeioPagamento("   -", meioPagamento);
            }
        }

        imprimirMensagem(" - Transações (Total: %d)", usuario.getQtdTransacoes());
        if (!respostaTransacoes.isSucesso()) {
            imprimirMensagem("   - * ERRO ao obter transações! (Código: %s, Mensagem: %s)",
                respostaTransacoes.getIdErro(),
                respostaTransacoes.getMensagemErro());

        } else {
            for (MensagemTransacao transacao : respostaTransacoes.getTransacoes()) {
                imprimirTransacao("   -", transacao);
            }
        }
    }

    private void cadastrarMeioPagamento() {
        Optional<MensagemUsuario> optUsuario = lerUsuario();
        if (!optUsuario.isPresent()) return;
        MensagemUsuario usuario = optUsuario.get();

        String numeroCartao;
        String cvvCartao;
        Optional<Integer> mesVencimento = Optional.empty();
        Optional<Integer> anoVencimento = Optional.empty();

        imprimirMensagem("Digite o número do cartão de crédito, com 16 dígitos: ");
        numeroCartao = lerLinha();

        imprimirMensagem("Digite o CVV do cartão de crédito, com 3 dígitos: ");
        cvvCartao = lerLinha();

        while (!mesVencimento.isPresent()) {
            imprimirMensagem("Digite o mês de vencimento do cartão, com 2 dígitos: ");
            mesVencimento = lerInt();
            if (!mesVencimento.isPresent()) imprimirMensagem("Número inválido.");
        }

        while (!anoVencimento.isPresent()) {
            imprimirMensagem("Digite o ano de vencimento do cartão, com 4 dígitos: ");
            anoVencimento = lerInt();
            if (!anoVencimento.isPresent()) imprimirMensagem("Número inválido.");
        }

        // Preparar e efetuar chamada
        RequisicaoAdicionarMeioPagamento requisicao = new RequisicaoAdicionarMeioPagamento();
        requisicao.setIdUsuario(usuario.getId());
        requisicao.setNumeroCartao(numeroCartao);
        requisicao.setCvvCartao(cvvCartao);
        requisicao.setMesVencimento(mesVencimento.get());
        requisicao.setAnoVencimento(anoVencimento.get());

        RespostaAdicionarMeioPagamento resposta = servico.adicionarMeioPagamento(requisicao);
        if (resposta.isSucesso()) {
            imprimirMensagem("Meio de pagamento (ID:%d) criado com sucesso.", resposta.getIdMeioPagamento());

        } else {
            imprimirErroRequisicao("Um erro ocorreu ao tentar criar o meio de pagamento.", resposta);
        }
    }

    private void iniciarTransacao() {
        Optional<List<MensagemMeioPagamento>> meiosPagamento = lerMeiosPagamento("");
        if (!meiosPagamento.isPresent()) return;

        if (meiosPagamento.get().size() == 0) {
            imprimirMensagem("O usuário informado não possui nenhum meio de pagamento cadastrado.");
            return;
        }

        Optional<Integer> indice = Optional.empty();
        while (!indice.isPresent()) {
            imprimirMensagem("Há %d meio(s) de pagamento disponíveis para uso na transação:", meiosPagamento.get().size());
            for (int i = 0; i < meiosPagamento.get().size(); ++i) {
                String prefixoMensagem = String.format("(Opção %d) ", i + 1);
                MensagemMeioPagamento meioPagamento = meiosPagamento.get().get(i);
                imprimirMeioPagamento(prefixoMensagem, meioPagamento);
            }

            imprimirMensagem("Selecione um meio de pagamento (pelo número da opção): ");
            indice = lerInt();
            if (!indice.isPresent() || indice.get() < 1 || indice.get() > meiosPagamento.get().size()) {
                imprimirMensagem("* Opção inválida.");
                indice = Optional.empty();
            }
        }
        MensagemMeioPagamento meioPagamento = meiosPagamento.get().get(indice.get() - 1);

        Optional<BigDecimal> valor = Optional.empty();
        while (!valor.isPresent()) {
            imprimirMensagem("Digite o valor da transação: ");
            valor = lerValorMonetario();
            if (!valor.isPresent()) {
                imprimirMensagem("* Valor inválido.");
                valor = Optional.empty();
            }
        }

        RequisicaoIniciarTransacao requisicao = new RequisicaoIniciarTransacao();
        requisicao.setIdMeioPagamento(meioPagamento.getIdMeioPagamento());
        requisicao.setValor(valor.get());

        RespostaIniciarTransacao resposta = servico.iniciarTransacao(requisicao);
        if (resposta.isSucesso()) {
            imprimirMensagem("Transação (ID:%d) iniciada com sucesso.", resposta.getIdTransacao());

        } else {
            imprimirErroRequisicao("Um erro ocorreu ao tentar iniciar a transação.", resposta);
        }
    }

    private Optional<List<MensagemMeioPagamento>> lerMeiosPagamento(String prefixo) {
        Optional<MensagemUsuario> optUsuario = lerUsuario();

        if (!optUsuario.isPresent()) return Optional.empty();

        return lerMeiosPagamento(prefixo, optUsuario.get().getId());
    }

    private Optional<List<MensagemMeioPagamento>> lerMeiosPagamento(String prefixo, Long idUsuario) {
        RequisicaoListarMeiosPagamento requisicaoMeiosPagamento = new RequisicaoListarMeiosPagamento();
        requisicaoMeiosPagamento.setIdUsuario(idUsuario);

        RespostaListarMeiosPagamento respostaMeiosPagamento = servico.listarMeiosPagamento(requisicaoMeiosPagamento);

        if (!respostaMeiosPagamento.isSucesso()) {
            imprimirMensagem(prefixo + "* ERRO ao obter meios de pagamento! (Código: %s, Mensagem: %s)",
                respostaMeiosPagamento.getIdErro(),
                respostaMeiosPagamento.getMensagemErro());
            return Optional.empty();

        } else {
            return Optional.of(respostaMeiosPagamento.getMeiosPagamento());
        }
    }

    private Optional<MensagemUsuario> lerUsuario() {
        RequisicaoBuscarUsuario requisicao = new RequisicaoBuscarUsuario();
        String nomeIdUsuario;
        Long idUsuario = null;
        Optional<MensagemUsuario> usuario = Optional.empty();

        // Obter dados
        imprimirMensagem("Digite o ID ou nome do usuário: ");
        nomeIdUsuario = lerLinha();
        if (nomeIdUsuario.matches("[0-9]+")) {
            idUsuario = Long.parseLong(nomeIdUsuario);
        }

        // Preparar e executar chamada
        requisicao.setIdUsuario(idUsuario);
        requisicao.setNomeUsuario(nomeIdUsuario);
        RespostaBuscarUsuario resposta = servico.buscarUsuario(requisicao);

        if (!resposta.isSucesso()) {
            imprimirErroRequisicao("Um erro ocorreu ao buscar o usuário.", resposta);

        } else if (!resposta.isEncontrado()) {
            imprimirMensagem("Usuário com nome/ID '%s' não existe.", nomeIdUsuario);

        } else {
            usuario = Optional.of(resposta.getUsuario());
        }

        return usuario;
    }

    private void imprimirOpcao(Operacao operacao) {
        imprimirMensagem("(%d) %s", operacao.id, operacao.nome);
    }

}
