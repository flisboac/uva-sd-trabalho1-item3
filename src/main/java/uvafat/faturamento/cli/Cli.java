package uvafat.faturamento.cli;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmiClient;
import uvafat.faturamento.cli.provedores.ProvedorServicosCli;

import java.rmi.RemoteException;

public class Cli implements AutoCloseable {

    private enum Modo {
        LOCAL("local"),
        CLIENTE("cliente"),
        SERVIDOR("servidor");

        public final String nome;

        Modo(String nome) {
            this.nome = nome;
        }

        public static Modo buscarPorNome(String nome) {
            for (Modo modo : values()) if (modo.nome.equals(nome)) return modo;
            return null;
        }
    }

    private String[] args;
    private ProvedorServicosCli provedor;

    public Cli(ProvedorServicosCli provedor, String[] args) {
        this.provedor = provedor;
        this.args = args;
    }

    public void run() {
        Modo modo = args.length > 0 ? Modo.buscarPorNome(args[0]) : Modo.LOCAL;

        if (modo == null) {
            System.err.println("* ERRO: Modo de execução '" + args[0] +"' desconhecido!");
            return;
        }

        switch(modo) {
        case LOCAL:
            ServicoFaturamento servicoLocal = provedor.instanciar();
            new CliCliente(servicoLocal).run();
            break;

        case CLIENTE:
            ServicoFaturamentoRmi.OpcoesConexao opcoesCliente = new ServicoFaturamentoRmi.OpcoesConexao();
            try (ServicoFaturamentoRmiClient cliente = provedor.conectar(opcoesCliente)) {
                new CliCliente(cliente).run();
            }
            break;

        case SERVIDOR:
            ServicoFaturamentoRmi.OpcoesServico opcoesServico = new ServicoFaturamentoRmi.OpcoesServico();
            try (ServicoFaturamentoRmi servico = provedor.iniciar(opcoesServico)) {
                new CliServidor(servico).run();

            } catch (Exception e) {
                e.printStackTrace();
            }
            break;
        }
    }

    @Override
    public void close() {
        provedor.close();
    }
}
