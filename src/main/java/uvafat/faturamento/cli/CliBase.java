package uvafat.faturamento.cli;

import uvafat.faturamento.aplicacao.api.mensagens.MensagemMeioPagamento;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemTransacao;
import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.Resposta;

import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Optional;
import java.util.Scanner;

public abstract class CliBase implements Runnable {

    protected InputStream in = System.in;
    protected PrintStream out = System.out;
    protected Scanner inScanner;

    @Override
    public void run() {
        try {
            inicializar();
            executar();

        } catch (Exception e) {
            e.printStackTrace(out);

        } finally {
            finalizar();
        }
    }

    private void inicializar() {
        inScanner = new Scanner(in);
    }

    private void finalizar() {
        if (inScanner != null) inScanner.close();
    }

    protected abstract void executar() throws RemoteException;

    protected void imprimirMensagem() {
        imprimirMensagem("");
    }

    protected void imprimirMensagem(String formato, Object... argumentos) {
        String mensagem = String.format(formato, argumentos);
        out.println(mensagem);
    }

    protected void imprimirErroRequisicao(String s, Resposta resposta) {
        String mensagem = String.format("%s (Código: %d; Mensagem: %s)", s, resposta.getIdErro(), resposta.getMensagemErro());
        out.println(mensagem);
    }

    protected void imprimirMeioPagamento(String prefixo, MensagemMeioPagamento meioPagamento) {
        String mensagem = String.format("%s[ID:%d]: Número do Cartão: %s, CVV: %s, Data de Vencimento: %02d/%04d",
            prefixo,
            meioPagamento.getIdMeioPagamento(),
            meioPagamento.getNumeroCartao(),
            meioPagamento.getCvvCartao(),
            meioPagamento.getMesVencimento(),
            meioPagamento.getAnoVencimento());
        out.println(mensagem);
    }

    protected void imprimirTransacao(String prefixo, MensagemTransacao transacao) {
        String mensagem = String.format("%s[ID:%d] ID Meio de pagamento: %d, Valor: %.03f, Estado: %s, Início: %s",
            prefixo,
            transacao.getIdTransacao(),
            transacao.getIdMeioPagamento(),
            transacao.getValor(),
            transacao.getNomeEstado(),
            transacao.getDataInicio().toLocalDate().toString()
        );

        if (transacao.isTerminada()) {
            mensagem = String.format("%s, Término: %s", mensagem, transacao.getDataTermino().toLocalDateTime().toString());
        }

        out.println(mensagem);
    }

    protected String lerLinha() {
        return inScanner.nextLine();
    }

    protected Optional<Integer> lerInt() {
        Optional<Integer> intg = Optional.empty();
        try {
             intg = Optional.of(inScanner.nextInt());
             inScanner.nextLine();

        } catch (InputMismatchException e) {
            inScanner.nextLine();
        }

        return intg;
    }

    protected Optional<BigDecimal> lerValorMonetario() {
        Optional<BigDecimal> valor = Optional.empty();
        try {
            valor = Optional.of(inScanner.nextBigDecimal());
            inScanner.nextLine();

        } catch (InputMismatchException e) {
            inScanner.nextLine();
        }
        return valor;
    }
}
