package uvafat.faturamento.aplicacao.impl.local;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemMeioPagamento;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemTransacao;
import uvafat.faturamento.aplicacao.api.mensagens.MensagemUsuario;
import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.DadosMeioPagamento;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.CvvCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.DadosCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.NumeroCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.VencimentoCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;
import uvafat.faturamento.negocio.api.modelo.usuarios.DadosUsuario;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RegistroMeioPagamento;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RepositorioMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RegistroTransacao;
import uvafat.faturamento.negocio.api.servico.usuarios.RegistroUsuario;
import uvafat.faturamento.negocio.api.servico.usuarios.RepositorioUsuario;
import uvafat.faturamento.negocio.api.suporte.IdErro;
import uvafat.faturamento.negocio.api.suporte.excecoes.Excecao;
import uvafat.faturamento.negocio.impl.memoria.servico.Repositorios;

import java.util.ArrayList;
import java.util.Optional;

public class ServicoFaturamentoLocal implements ServicoFaturamento {

    private RepositorioUsuario repositorioUsuario;
    private RepositorioMeioPagamento repositorioMeioPagamento;

    public ServicoFaturamentoLocal(RepositorioUsuario repositorioUsuario, RepositorioMeioPagamento repositorioMeioPagamento) {
        this.repositorioUsuario = repositorioUsuario;
        this.repositorioMeioPagamento = repositorioMeioPagamento;
    }

    @Override
    public RespostaCriarUsuario criarUsuario(RequisicaoCriarUsuario requisicao) {
        RespostaCriarUsuario resposta = new RespostaCriarUsuario();

        try {
            RegistroUsuario registro = repositorioUsuario.criar(requisicao.getNome());
            resposta.setIdUsuario(registro.getId().getValor());
            resposta.setSucesso(true);
            resposta.setIdUsuario(registro.getId().getValor());

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    @Override
    public RespostaBuscarUsuario buscarUsuario(RequisicaoBuscarUsuario requisicao) {
        RespostaBuscarUsuario resposta = new RespostaBuscarUsuario();

        try {
            Usuario.Id idUsuario = new Usuario.Id(requisicao.getIdUsuario());
            Usuario usuario = new DadosUsuario(idUsuario, requisicao.getNomeUsuario());
            Optional<RegistroUsuario> registro = repositorioUsuario.buscar(usuario);
            resposta.setSucesso(true);

            if (!registro.isPresent()) {
                resposta.setEncontrado(false);

            } else {
                MensagemUsuario mensagem = criarMensagem(registro.get());
                resposta.setEncontrado(true);
                resposta.setUsuario(mensagem);
            }

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    @Override
    public RespostaAdicionarMeioPagamento adicionarMeioPagamento(RequisicaoAdicionarMeioPagamento requisicao) {
        RespostaAdicionarMeioPagamento resposta = new RespostaAdicionarMeioPagamento();

        try {
            Usuario.Id idUsuario = new Usuario.Id(requisicao.getIdUsuario());
            Usuario usuario = new DadosUsuario(idUsuario);
            Optional<RegistroUsuario> registro = repositorioUsuario.buscar(usuario);

            if (!registro.isPresent()) {
                resposta.setSucesso(false);
                resposta.setIdErro(IdErro.ERRO.getId());
                resposta.setMensagemErro("Usuário não encontrado.");

            } else {
                String numeroCartao = requisicao.getNumeroCartao();
                String cvvCartao = requisicao.getCvvCartao();
                DadosCartaoCredito dadosCartaoCredito = DadosCartaoCredito.criar(
                    NumeroCartaoCredito.paraPersistencia(numeroCartao),
                    CvvCartaoCredito.paraPersistencia(cvvCartao),
                    VencimentoCartaoCredito.em(requisicao.getMesVencimento(), requisicao.getAnoVencimento())
                );
                RegistroMeioPagamento meioPagamento = registro.get().addMeioPagamento(dadosCartaoCredito);

                resposta.setSucesso(true);
                resposta.setIdMeioPagamento(meioPagamento.getId().getValor());
            }

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    @Override
    public RespostaListarMeiosPagamento listarMeiosPagamento(RequisicaoListarMeiosPagamento requisicao) {
        RespostaListarMeiosPagamento resposta = new RespostaListarMeiosPagamento();

        try {
            Usuario.Id idUsuario = new Usuario.Id(requisicao.getIdUsuario());
            Usuario usuario = new DadosUsuario(idUsuario);
            Optional<RegistroUsuario> registro = repositorioUsuario.buscar(usuario);

            if (!registro.isPresent()) {
                resposta.setSucesso(false);
                resposta.setIdErro(IdErro.ERRO.getId());
                resposta.setMensagemErro("Usuário não encontrado.");

            } else {
                resposta.setSucesso(true);
                resposta.setMeiosPagamento(new ArrayList<>());
                for (MeioPagamento meioPagamento : registro.get().getMeiosPagamento()) {
                    MensagemMeioPagamento mensagem = criarMensagem(meioPagamento);
                    resposta.getMeiosPagamento().add(mensagem);
                }
            }

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    @Override
    public RespostaIniciarTransacao iniciarTransacao(RequisicaoIniciarTransacao requisicao) {
        RespostaIniciarTransacao resposta = new RespostaIniciarTransacao();

        try {
            MeioPagamento.Id idMeioPagamento = new MeioPagamento.Id(requisicao.getIdMeioPagamento());
            MeioPagamento meioPagamento = new DadosMeioPagamento(idMeioPagamento);
            Optional<RegistroMeioPagamento> registroMeioPagamento = repositorioMeioPagamento.buscar(meioPagamento);

            if (!registroMeioPagamento.isPresent()) {
                resposta.setSucesso(false);
                resposta.setIdErro(IdErro.ERRO.getId());
                resposta.setMensagemErro("Meio de pagamento não encontrado.");

            } else {
                RegistroUsuario registroUsuario = registroMeioPagamento.get().getUsuario();
                RegistroTransacao registroTransacao = registroUsuario.iniciarTransacao(registroMeioPagamento.get(), requisicao.getValor());

                resposta.setSucesso(true);
                resposta.setIdTransacao(registroTransacao.getId().getValor());
            }

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    @Override
    public RespostaListarTransacoes listarTransacoes(RequisicaoListarTransacoes requisicao) {
        RespostaListarTransacoes resposta = new RespostaListarTransacoes();

        try {
            Usuario.Id idUsuario = new Usuario.Id(requisicao.getIdUsuario());
            Usuario usuario = new DadosUsuario(idUsuario);
            Optional<RegistroUsuario> registro = repositorioUsuario.buscar(usuario);

            if (!registro.isPresent()) {
                resposta.setSucesso(false);
                resposta.setIdErro(IdErro.ERRO.getId());
                resposta.setMensagemErro("Usuário inexistente.");

            } else {
                resposta.setSucesso(true);
                resposta.setTransacoes(new ArrayList<>());
                for (Transacao transacao : registro.get().getTransacoes()) {
                    MensagemTransacao mensagem = criarMensagem(transacao);
                    resposta.getTransacoes().add(mensagem);
                }
            }

        } catch (Excecao e) {
            resposta.setSucesso(false);
            resposta.setIdErro(e.getIdErro().getId());
            resposta.setMensagemErro(e.getMessage());

        } catch (Exception e) {
            resposta.setSucesso(false);
            resposta.setIdErro(IdErro.ERRO.getId());
            resposta.setMensagemErro(e.getMessage());
        }

        return resposta;
    }

    private MensagemUsuario criarMensagem(RegistroUsuario usuario) {
        MensagemUsuario mensagem = new MensagemUsuario();
        mensagem.setId(usuario.getId().getValor());
        mensagem.setNome(usuario.getNome());
        mensagem.setQtdMeiosPagamento(usuario.getQtdMeiosPagamento());
        mensagem.setQtdTransacoes(usuario.getQtdTransacoes());
        return mensagem;
    }

    private MensagemMeioPagamento criarMensagem(MeioPagamento meioPagamento) {
        MensagemMeioPagamento mensagem = new MensagemMeioPagamento();
        mensagem.setIdUsuario(meioPagamento.getUsuario().getId().getValor());
        mensagem.setIdMeioPagamento(meioPagamento.getId().getValor());
        mensagem.setNumeroCartao(meioPagamento.getDadosCartaoCredito().getNumero().getValor());
        mensagem.setCvvCartao(meioPagamento.getDadosCartaoCredito().getCvv().getValor());
        mensagem.setMesVencimento(meioPagamento.getDadosCartaoCredito().getVencimento().getMes().getValue());
        mensagem.setAnoVencimento(meioPagamento.getDadosCartaoCredito().getVencimento().getAno());
        return mensagem;
    }

    private MensagemTransacao criarMensagem(Transacao transacao) {
        MensagemTransacao mensagem = new MensagemTransacao();
        mensagem.setIdTransacao(transacao.getId().getValor());
        mensagem.setIdMeioPagamento(transacao.getMeioPagamento().getId().getValor());
        mensagem.setIdUsuario(transacao.getMeioPagamento().getUsuario().getId().getValor());
        mensagem.setIdEstado(transacao.getEstado().getId());
        mensagem.setNomeEstado(transacao.getEstado().getNome());
        mensagem.setTerminada(transacao.getEstado().isTerminal());
        mensagem.setValor(transacao.getValor());
        mensagem.setDataInicio(transacao.getDataInicio());
        mensagem.setDataTermino(transacao.getDataTermino().orElse(null));
        return mensagem;
    }

    public static ServicoFaturamentoLocal instanciar() {
        return new ServicoFaturamentoLocal(
            Repositorios.para(RepositorioUsuario.class),
            Repositorios.para(RepositorioMeioPagamento.class)
        );
    }
}
