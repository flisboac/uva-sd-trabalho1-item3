package uvafat.faturamento.aplicacao.impl.rmi;

import uvafat.faturamento.aplicacao.api.FalhaRmi;
import uvafat.faturamento.aplicacao.api.ServicoFaturamento;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;
import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;
import uvafat.faturamento.aplicacao.impl.local.ServicoFaturamentoLocal;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class ServicoFaturamentoRmiImpl implements ServicoFaturamentoRmi {

    private Registry registroRmi;
    private String nomeObjeto;
    private ServicoFaturamento servicoImpl;

    private int contadorObservadores = 0;
    private Map<Integer, Observador> observadores = new HashMap<>();

    public ServicoFaturamentoRmiImpl(Registry registroRmi, String nomeObjeto, ServicoFaturamento servicoImpl) {
        this.registroRmi = registroRmi;
        this.nomeObjeto = nomeObjeto;
        this.servicoImpl = servicoImpl;
    }

    @Override
    public RespostaCriarUsuario criarUsuario(RequisicaoCriarUsuario requisicao) throws RemoteException {
        RespostaCriarUsuario resposta = servicoImpl.criarUsuario(requisicao);
        if (resposta.isSucesso()) notificar(new Evento(TipoEvento.USUARIO_ADICIONADO, resposta.getIdUsuario()));
        return resposta;
    }

    @Override
    public RespostaBuscarUsuario buscarUsuario(RequisicaoBuscarUsuario requisicao) throws RemoteException {
        return servicoImpl.buscarUsuario(requisicao);
    }

    @Override
    public RespostaAdicionarMeioPagamento adicionarMeioPagamento(RequisicaoAdicionarMeioPagamento requisicao) throws RemoteException {
        RespostaAdicionarMeioPagamento resposta = servicoImpl.adicionarMeioPagamento(requisicao);
        if (resposta.isSucesso()) notificar(new Evento(TipoEvento.MEIO_PAGAMENTO_ADICIONADO, resposta.getIdMeioPagamento()));
        return resposta;
    }

    @Override
    public RespostaListarMeiosPagamento listarMeiosPagamento(RequisicaoListarMeiosPagamento requisicao) throws RemoteException {
        return servicoImpl.listarMeiosPagamento(requisicao);
    }

    @Override
    public RespostaIniciarTransacao iniciarTransacao(RequisicaoIniciarTransacao requisicao) throws RemoteException {
        RespostaIniciarTransacao resposta = servicoImpl.iniciarTransacao(requisicao);
        if (resposta.isSucesso()) notificar(new Evento(TipoEvento.TRANSACAO_ADICIONADA, resposta.getIdTransacao()));
        return resposta;
    }

    @Override
    public RespostaListarTransacoes listarTransacoes(RequisicaoListarTransacoes requisicao) throws RemoteException {
        return servicoImpl.listarTransacoes(requisicao);
    }

    @Override
    public Object observar(Observador observador) {
        int idObservador = contadorObservadores++;
        observadores.put(idObservador, observador);
        return idObservador;
    }

    @Override
    public void removerObservador(Object chave) {
        Objects.requireNonNull(chave);
        Integer idObservador = Integer.class.cast(chave);
        observadores.remove(idObservador);
    }

    @Override
    public RespostaFinalizarTransacao finalizarTransacao(RequisicaoFinalizarTransacao requisicao) throws RemoteException {
        return null;
    }

    @Override
    public void close() {

        if (this.registroRmi != null) {
            try {
                this.registroRmi.unbind(this.nomeObjeto);
                UnicastRemoteObject.unexportObject(this, false);
                this.registroRmi = null;
                this.servicoImpl = null;

            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void notificar(Evento evento) {
        for (Observador observador : observadores.values()) {
            observador.notificar(evento);
        }
    }

    public static ServicoFaturamentoRmi iniciar(OpcoesServico opcoes) throws FalhaRmi {

        try {
            String nomeObjeto = opcoes.getNomeObjeto();
            Registry registry = LocateRegistry.getRegistry();
            ServicoFaturamentoRmiImpl instancia = new ServicoFaturamentoRmiImpl(registry, nomeObjeto, ServicoFaturamentoLocal.instanciar());
            ServicoFaturamentoRmi stub = (ServicoFaturamentoRmi) UnicastRemoteObject.exportObject(instancia, opcoes.getPorta());
            registry.rebind(nomeObjeto, stub);
            return stub;

        } catch (Exception e) {
            throw new FalhaRmi(e);
        }
    }

}
