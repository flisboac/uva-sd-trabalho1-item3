package uvafat.faturamento.aplicacao.impl.rmi;

import uvafat.faturamento.aplicacao.api.FalhaRmi;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmi;
import uvafat.faturamento.aplicacao.api.ServicoFaturamentoRmiClient;
import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServicoFaturamentoRmiClientImpl implements ServicoFaturamentoRmiClient {

    private ServicoFaturamentoRmi servico;

    public ServicoFaturamentoRmiClientImpl(ServicoFaturamentoRmi servico) {
        this.servico = servico;
    }

    @Override
    public RespostaCriarUsuario criarUsuario(RequisicaoCriarUsuario requisicao) {
        try {
            return servico.criarUsuario(requisicao);

        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public RespostaBuscarUsuario buscarUsuario(RequisicaoBuscarUsuario requisicao) {
        try {
            return servico.buscarUsuario(requisicao);
        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public RespostaAdicionarMeioPagamento adicionarMeioPagamento(RequisicaoAdicionarMeioPagamento requisicao) {
        try {
            return servico.adicionarMeioPagamento(requisicao);
        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public RespostaListarMeiosPagamento listarMeiosPagamento(RequisicaoListarMeiosPagamento requisicao) {
        try {
            return servico.listarMeiosPagamento(requisicao);
        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public RespostaIniciarTransacao iniciarTransacao(RequisicaoIniciarTransacao requisicao) {
        try {
            return servico.iniciarTransacao(requisicao);
        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public RespostaListarTransacoes listarTransacoes(RequisicaoListarTransacoes requisicao) {
        try {
            return servico.listarTransacoes(requisicao);
        } catch (RemoteException e) {
            throw new FalhaRmi(e);
        }
    }

    @Override
    public void close() {
        if (servico != null) {
            try {
                servico.close();
            } catch (RemoteException e) {
            }
            servico = null;
        }
    }

    public static ServicoFaturamentoRmiClientImpl conectar(ServicoFaturamentoRmi.OpcoesConexao opcoes) {
        try {
            String nomeObjeto = opcoes.getNomeObjeto();
            String hostname = opcoes.getHostname();
            int porta = opcoes.getPorta();
            Registry registry = LocateRegistry.getRegistry();
            ServicoFaturamentoRmi stub = (ServicoFaturamentoRmi) registry.lookup(nomeObjeto);
            ServicoFaturamentoRmiClientImpl cliente = new ServicoFaturamentoRmiClientImpl(stub);
            return cliente;

        } catch (RemoteException | NotBoundException e) {
            throw new FalhaRmi(e);
        }
    }
}
