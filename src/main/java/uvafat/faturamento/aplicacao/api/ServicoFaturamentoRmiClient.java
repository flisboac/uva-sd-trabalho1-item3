package uvafat.faturamento.aplicacao.api;

import uvafat.faturamento.aplicacao.api.ServicoFaturamento;

public interface ServicoFaturamentoRmiClient extends ServicoFaturamento, AutoCloseable {

    @Override
    void close();
}
