package uvafat.faturamento.aplicacao.api;

import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;

public interface ServicoFaturamento {

    // Métodos de usuário
    RespostaCriarUsuario criarUsuario(RequisicaoCriarUsuario requisicao);
    RespostaBuscarUsuario buscarUsuario(RequisicaoBuscarUsuario resposta);

    // Métodos de meios de pagamento
    RespostaAdicionarMeioPagamento adicionarMeioPagamento(RequisicaoAdicionarMeioPagamento requisicao);
    RespostaListarMeiosPagamento listarMeiosPagamento(RequisicaoListarMeiosPagamento requisicao);

    // Métodos de transação
    RespostaIniciarTransacao iniciarTransacao(RequisicaoIniciarTransacao requisicao);
    RespostaListarTransacoes listarTransacoes(RequisicaoListarTransacoes requisicao);
}
