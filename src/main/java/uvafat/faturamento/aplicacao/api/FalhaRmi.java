package uvafat.faturamento.aplicacao.api;

import uvafat.faturamento.negocio.api.suporte.IdErro;
import uvafat.faturamento.negocio.api.suporte.excecoes.Excecao;

public class FalhaRmi extends Excecao {

    public FalhaRmi() {
    }

    public FalhaRmi(String s) {
        super(s);
    }

    public FalhaRmi(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FalhaRmi(Throwable throwable) {
        super(throwable);
    }

    public FalhaRmi(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public FalhaRmi(IdErro idErro) {
        super(idErro);
    }

    public FalhaRmi(IdErro idErro, String mensagemErro) {
        super(idErro, mensagemErro);
    }
}
