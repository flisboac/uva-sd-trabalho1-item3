package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

public class RequisicaoFinalizarTransacao extends Requisicao {

    private Long idTransacao;
    private boolean aprovada;

    public RequisicaoFinalizarTransacao() {
    }

    public Long getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(Long idTransacao) {
        this.idTransacao = idTransacao;
    }

    public boolean isAprovada() {
        return aprovada;
    }

    public void setAprovada(boolean aprovada) {
        this.aprovada = aprovada;
    }
}
