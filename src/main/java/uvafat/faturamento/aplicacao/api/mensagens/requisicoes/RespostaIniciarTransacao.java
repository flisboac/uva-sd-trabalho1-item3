package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RespostaIniciarTransacao extends Resposta {

    public Long idTransacao;

    public RespostaIniciarTransacao() {
    }

    public Long getIdTransacao() {
        Objects.requireNonNull(idTransacao);
        return idTransacao;
    }

    public void setIdTransacao(Long idTransacao) {
        this.idTransacao = idTransacao;
    }
}
