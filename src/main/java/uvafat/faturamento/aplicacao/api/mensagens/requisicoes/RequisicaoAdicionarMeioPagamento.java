package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RequisicaoAdicionarMeioPagamento extends Requisicao {

    private Long idUsuario;
    private String numeroCartao;
    private String cvvCartao;
    private Integer mesVencimento;
    private Integer anoVencimento;

    public RequisicaoAdicionarMeioPagamento() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public String getCvvCartao() {
        return cvvCartao;
    }

    public void setCvvCartao(String cvvCartao) {
        this.cvvCartao = cvvCartao;
    }

    public Integer getMesVencimento() {
        return mesVencimento;
    }

    public void setMesVencimento(Integer mesVencimento) {
        this.mesVencimento = mesVencimento;
    }

    public Integer getAnoVencimento() {
        return anoVencimento;
    }

    public void setAnoVencimento(Integer anoVencimento) {
        this.anoVencimento = anoVencimento;
    }
}
