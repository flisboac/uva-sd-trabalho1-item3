package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RequisicaoBuscarUsuario extends Requisicao {

    private Long idUsuario;
    private String nomeUsuario;

    public RequisicaoBuscarUsuario() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }
}
