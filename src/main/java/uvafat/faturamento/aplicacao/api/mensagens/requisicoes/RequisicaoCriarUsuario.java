package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RequisicaoCriarUsuario extends Requisicao {

    private String nome;

    public RequisicaoCriarUsuario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
