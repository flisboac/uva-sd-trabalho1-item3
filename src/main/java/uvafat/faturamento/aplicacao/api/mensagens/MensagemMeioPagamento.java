package uvafat.faturamento.aplicacao.api.mensagens;

public class MensagemMeioPagamento extends Mensagem {

    private Long idUsuario;
    private Long idMeioPagamento;
    private String numeroCartao;
    private String cvvCartao;
    private Integer mesVencimento;
    private Integer anoVencimento;

    public MensagemMeioPagamento() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdMeioPagamento() {
        return idMeioPagamento;
    }

    public void setIdMeioPagamento(Long idMeioPagamento) {
        this.idMeioPagamento = idMeioPagamento;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public String getCvvCartao() {
        return cvvCartao;
    }

    public void setCvvCartao(String cvvCartao) {
        this.cvvCartao = cvvCartao;
    }

    public Integer getMesVencimento() {
        return mesVencimento;
    }

    public void setMesVencimento(Integer mesVencimento) {
        this.mesVencimento = mesVencimento;
    }

    public Integer getAnoVencimento() {
        return anoVencimento;
    }

    public void setAnoVencimento(Integer anoVencimento) {
        this.anoVencimento = anoVencimento;
    }
}
