package uvafat.faturamento.aplicacao.api.mensagens;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class MensagemTransacao extends Mensagem  {

    private Long idUsuario;
    private Long idTransacao;
    private Long idMeioPagamento;
    private BigDecimal valor;
    private int idEstado;
    private String nomeEstado;
    private boolean terminada;
    private ZonedDateTime dataInicio;
    private ZonedDateTime dataTermino;

    public MensagemTransacao() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(Long idTransacao) {
        this.idTransacao = idTransacao;
    }

    public Long getIdMeioPagamento() {
        return idMeioPagamento;
    }

    public void setIdMeioPagamento(Long idMeioPagamento) {
        this.idMeioPagamento = idMeioPagamento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int estado) {
        this.idEstado = estado;
    }

    public boolean isTerminada() {
        return terminada;
    }

    public void setTerminada(boolean terminada) {
        this.terminada = terminada;
    }

    public String getNomeEstado() {
        return nomeEstado;
    }

    public void setNomeEstado(String nomeEstado) {
        this.nomeEstado = nomeEstado;
    }

    public ZonedDateTime getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(ZonedDateTime dataInicio) {
        this.dataInicio = dataInicio;
    }

    public ZonedDateTime getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(ZonedDateTime dataTermino) {
        this.dataTermino = dataTermino;
    }
}
