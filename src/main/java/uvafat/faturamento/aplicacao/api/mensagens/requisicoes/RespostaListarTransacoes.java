package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import uvafat.faturamento.aplicacao.api.mensagens.MensagemTransacao;

import java.util.List;
import java.util.Objects;

public class RespostaListarTransacoes extends Resposta {

    private List<MensagemTransacao> transacoes;

    public RespostaListarTransacoes() {
    }

    public List<MensagemTransacao> getTransacoes() {
        Objects.requireNonNull(transacoes);
        return transacoes;
    }

    public void setTransacoes(List<MensagemTransacao> transacoes) {
        this.transacoes = transacoes;
    }
}
