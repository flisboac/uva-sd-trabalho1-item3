package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.DadosMeioPagamento;

import java.util.List;
import java.util.Objects;

public class RequisicaoListarMeiosPagamento extends Requisicao {

    private Long idUsuario;

    public RequisicaoListarMeiosPagamento() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
