package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.io.Serializable;
import java.util.Objects;

public abstract class Resposta implements Serializable {

    private boolean sucesso;
    private Integer idErro;
    private String mensagemErro;

    public Resposta() {
    }

    public Resposta(boolean sucesso, Integer idErro, String mensagemErro) {
        this.sucesso = sucesso;
        this.idErro = idErro;
        this.mensagemErro = mensagemErro;
    }

    public boolean isSucesso() {
        return sucesso;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public int getIdErro() {
        return idErro;
    }

    public void setIdErro(int idErro) {
        this.idErro = idErro;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }
}
