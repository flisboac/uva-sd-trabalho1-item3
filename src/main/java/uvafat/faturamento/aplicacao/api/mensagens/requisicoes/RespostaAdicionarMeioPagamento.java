package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RespostaAdicionarMeioPagamento extends Resposta {

    private Long idMeioPagamento;

    public RespostaAdicionarMeioPagamento() {
    }

    public Long getIdMeioPagamento() {
        Objects.requireNonNull(idMeioPagamento);
        return idMeioPagamento;
    }

    public void setIdMeioPagamento(Long idMeioPagamento) {
        this.idMeioPagamento = idMeioPagamento;
    }
}
