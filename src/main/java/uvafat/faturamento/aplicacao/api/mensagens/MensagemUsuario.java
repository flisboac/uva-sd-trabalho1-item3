package uvafat.faturamento.aplicacao.api.mensagens;

import java.util.Objects;

public class MensagemUsuario extends Mensagem {

    private Long id;
    private String nome;
    private Integer qtdMeiosPagamento;
    private Integer qtdTransacoes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        Objects.requireNonNull(nome);
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdMeiosPagamento() {
        Objects.requireNonNull(qtdMeiosPagamento);
        return qtdMeiosPagamento;
    }

    public void setQtdMeiosPagamento(Integer qtdMeiosPagamento) {
        this.qtdMeiosPagamento = qtdMeiosPagamento;
    }

    public Integer getQtdTransacoes() {
        Objects.requireNonNull(qtdTransacoes);
        return qtdTransacoes;
    }

    public void setQtdTransacoes(Integer qtdTransacoes) {
        this.qtdTransacoes = qtdTransacoes;
    }
}
