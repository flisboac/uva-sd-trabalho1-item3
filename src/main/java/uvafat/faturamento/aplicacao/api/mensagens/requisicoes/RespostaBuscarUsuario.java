package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import uvafat.faturamento.aplicacao.api.mensagens.MensagemUsuario;

import java.util.Objects;

public class RespostaBuscarUsuario extends Resposta {

    private boolean encontrado;
    private MensagemUsuario usuario;

    public RespostaBuscarUsuario() {
    }

    public boolean isEncontrado() {
        return encontrado;
    }

    public void setEncontrado(boolean encontrado) {
        this.encontrado = encontrado;
    }

    public MensagemUsuario getUsuario() {
        Objects.requireNonNull(usuario);
        return usuario;
    }

    public void setUsuario(MensagemUsuario usuario) {
        this.usuario = usuario;
    }
}
