package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RespostaCriarUsuario extends Resposta {

    private Long idUsuario;

    public RespostaCriarUsuario() {
    }

    public Long getIdUsuario() {
        Objects.requireNonNull(idUsuario);
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
