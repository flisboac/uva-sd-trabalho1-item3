package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import uvafat.faturamento.aplicacao.api.mensagens.MensagemMeioPagamento;

import java.util.List;
import java.util.Objects;

public class RespostaListarMeiosPagamento extends Resposta {

    private List<MensagemMeioPagamento> meiosPagamento;

    public RespostaListarMeiosPagamento() {
    }

    public List<MensagemMeioPagamento> getMeiosPagamento() {
        Objects.requireNonNull(meiosPagamento);
        return meiosPagamento;
    }

    public void setMeiosPagamento(List<MensagemMeioPagamento> meiosPagamento) {
        this.meiosPagamento = meiosPagamento;
    }
}
