package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.util.Objects;

public class RequisicaoListarTransacoes extends Requisicao {

    private Long idUsuario;

    public RequisicaoListarTransacoes() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
