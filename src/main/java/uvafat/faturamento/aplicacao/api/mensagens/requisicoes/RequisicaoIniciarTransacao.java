package uvafat.faturamento.aplicacao.api.mensagens.requisicoes;

import java.math.BigDecimal;
import java.util.Objects;

public class RequisicaoIniciarTransacao extends Requisicao {

    private Long idMeioPagamento;
    private BigDecimal valor;

    public RequisicaoIniciarTransacao() {
    }

    public Long getIdMeioPagamento() {
        return idMeioPagamento;
    }

    public void setIdMeioPagamento(Long idMeioPagamento) {
        this.idMeioPagamento = idMeioPagamento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
