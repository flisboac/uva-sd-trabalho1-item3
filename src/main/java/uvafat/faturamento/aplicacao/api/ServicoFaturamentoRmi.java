package uvafat.faturamento.aplicacao.api;

import uvafat.faturamento.aplicacao.api.mensagens.requisicoes.*;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServicoFaturamentoRmi extends Remote, AutoCloseable {

    String HOSTNAME_PADRAO = "localhost";
    String NOME_OBJETO_PADRAO = "ServicoFaturamentoRmi";
    int PORTA_OBJETO_PADRAO = 8081;

    class OpcoesConexao {

        private String hostname = HOSTNAME_PADRAO;
        private int porta = PORTA_OBJETO_PADRAO;
        private String nomeObjeto = NOME_OBJETO_PADRAO;

        public OpcoesConexao() {
        }

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public int getPorta() {
            return porta;
        }

        public void setPorta(int porta) {
            this.porta = porta;
        }

        public String getNomeObjeto() {
            return nomeObjeto;
        }

        public void setNomeObjeto(String nomeObjeto) {
            this.nomeObjeto = nomeObjeto;
        }
    }

    class OpcoesServico {

        private String nomeObjeto = NOME_OBJETO_PADRAO;
        private int porta = PORTA_OBJETO_PADRAO;

        public OpcoesServico() {
        }

        public int getPorta() {
            return porta;
        }

        public void setPorta(int porta) {
            this.porta = porta;
        }

        public String getNomeObjeto() {
            return nomeObjeto;
        }

        public void setNomeObjeto(String nomeObjeto) {
            this.nomeObjeto = nomeObjeto;
        }
    }

    enum TipoEvento {

        USUARIO_ADICIONADO(1),
        MEIO_PAGAMENTO_ADICIONADO(2),
        TRANSACAO_ADICIONADA(3);

        private final int id;

        TipoEvento(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public String getNome() {
            return name();
        }
    }

    class Evento {

        private TipoEvento tipo;
        private Long idEntidade;

        public Evento(TipoEvento tipo, Long idEntidade) {
            this.tipo = tipo;
            this.idEntidade = idEntidade;
        }

        public TipoEvento getTipo() {
            return tipo;
        }

        public Long getIdEntidade() {
            return idEntidade;
        }
    }

    interface Observador {

        void notificar(Evento evento);
    }

    // Métodos de usuário
    RespostaCriarUsuario criarUsuario(RequisicaoCriarUsuario requisicao) throws RemoteException;
    RespostaBuscarUsuario buscarUsuario(RequisicaoBuscarUsuario resposta) throws RemoteException;

    // Métodos de meios de pagamento
    RespostaAdicionarMeioPagamento adicionarMeioPagamento(RequisicaoAdicionarMeioPagamento requisicao) throws RemoteException;
    RespostaListarMeiosPagamento listarMeiosPagamento(RequisicaoListarMeiosPagamento requisicao) throws RemoteException;

    // Métodos de transação
    RespostaIniciarTransacao iniciarTransacao(RequisicaoIniciarTransacao requisicao) throws RemoteException;
    RespostaListarTransacoes listarTransacoes(RequisicaoListarTransacoes requisicao) throws RemoteException;

    // Métodos de servidor
    Object observar(Observador observador) throws RemoteException;
    void removerObservador(Object chave) throws RemoteException;
    RespostaFinalizarTransacao finalizarTransacao(RequisicaoFinalizarTransacao requisicao) throws RemoteException;

    @Override
    void close() throws RemoteException;
}
