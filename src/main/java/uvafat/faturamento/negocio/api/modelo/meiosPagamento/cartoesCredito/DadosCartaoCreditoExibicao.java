package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;

import java.util.Objects;

public class DadosCartaoCreditoExibicao implements DadosCartaoCredito {

    public static class Numero implements NumeroCartaoCredito {

        private String numero;

        Numero(String numero) {
            Objects.requireNonNull(numero, "Número não pode ser nulo.");

            // Remove espaços (início e fim) e pontuações
            numero = numero.trim().replace("[.]", "").toUpperCase();

            if (!numero.matches("[0-9]{4}[0-9xX]{8}[0-9]{4}")) {
                throw new ArgumentoInvalido("Número de cartão inválido para exibição.");
            }

            this.numero = numero.substring(0, 4) + "XXXXXXXX" + numero.substring(12,16);
        }

        public String getValor() {
            return this.numero;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Numero numero1 = (Numero) o;
            return Objects.equals(numero, numero1.numero);
        }

        @Override
        public int hashCode() {
            return Objects.hash(numero);
        }

        public String toString() {
            return this.numero;
        }
    }

    public static class Cvv implements CvvCartaoCredito {

        private String cvv = "XXX";

        public String getValor() {
            return cvv;
        }

        @Override
        public String toString() {
            return cvv;
        }

        public static final Cvv INSTANCIA = new Cvv();
    }

    private Numero numero;
    private VencimentoCartaoCredito vencimento;

    DadosCartaoCreditoExibicao(Numero numero, VencimentoCartaoCredito vencimento) {
        Objects.requireNonNull(numero, "Número do cartão de crédito não pode ser nulo.");
        Objects.requireNonNull(vencimento, "Vencimento do cartão de crédito não pode ser nulo.");
        this.numero = numero;
        this.vencimento = vencimento;
    }

    DadosCartaoCreditoExibicao(DadosCartaoCreditoPersistencia dados) {
        Objects.requireNonNull(numero, "Dados do cartão de crédito não pode ser nulo.");
        this.numero = new Numero(dados.getNumero().getValor());
        this.vencimento = dados.getVencimento();
    }

    @Override
    public boolean isOculto() {
        return true;
    }

    @Override
    public Numero getNumero() {
        return numero;
    }

    @Override
    public Cvv getCvv() {
        return Cvv.INSTANCIA;
    }

    @Override
    public VencimentoCartaoCredito getVencimento() {
        return vencimento;
    }
}
