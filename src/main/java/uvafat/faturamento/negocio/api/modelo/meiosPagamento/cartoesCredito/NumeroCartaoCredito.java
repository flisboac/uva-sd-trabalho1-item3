package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

public interface NumeroCartaoCredito {

    String getValor();

    static DadosCartaoCreditoPersistencia.Numero paraPersistencia(String numero) {
        return new DadosCartaoCreditoPersistencia.Numero(numero);
    }

    static NumeroCartaoCredito paraPersistencia(NumeroCartaoCredito numero) {
        return paraPersistencia(numero.getValor());
    }

    static NumeroCartaoCredito paraExibicao(String numero) {
        return new DadosCartaoCreditoExibicao.Numero(numero);
    }

    static NumeroCartaoCredito paraExibicao(NumeroCartaoCredito numero) {
        return paraExibicao(numero.getValor());
    }
}
