package uvafat.faturamento.negocio.api.modelo.transacoes;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.suporte.entidades.Entidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidadePadrao;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;

public interface Transacao extends Entidade {

    class Id extends IdEntidadePadrao<Long> implements IdEntidade {
        private Id() {}
        public Id(Long id) { super(id); }
        public static final Id NULO = new Id();
    }

    enum Estado {
        INICIADA(1, false),
        APROVADA(2, true),
        REJEITADA(3, true);

        private final int id;
        private final boolean terminal;

        Estado(int id, boolean terminal) {
            this.id = id;
            this.terminal = terminal;
        }

        public int getId() {
            return id;
        }

        public boolean isTerminal() {
            return this.terminal;
        }

        public String getNome() {
            return name();
        }
    }

    // Propriedades
    Id getId();
    MeioPagamento getMeioPagamento();
    Estado getEstado();
    BigDecimal getValor();
    ZonedDateTime getDataInicio();
    Optional<ZonedDateTime> getDataTermino();
}
