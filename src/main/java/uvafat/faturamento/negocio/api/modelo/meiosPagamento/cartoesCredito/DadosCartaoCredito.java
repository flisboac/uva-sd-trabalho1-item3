package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

public interface DadosCartaoCredito {

    static DadosCartaoCreditoPersistencia criar(
            DadosCartaoCreditoPersistencia.Numero numero,
            DadosCartaoCreditoPersistencia.Cvv cvv,
            VencimentoCartaoCredito vencimento) {
        return new DadosCartaoCreditoPersistencia(numero, cvv, vencimento);
    }

    static DadosCartaoCreditoExibicao criar(
            DadosCartaoCreditoExibicao.Numero numero,
            VencimentoCartaoCredito vencimento) {
        return new DadosCartaoCreditoExibicao(numero, vencimento);
    }

    static DadosCartaoCreditoExibicao paraExibicao(DadosCartaoCreditoPersistencia dados) {
        return new DadosCartaoCreditoExibicao(dados);
    }

    boolean isOculto();
    NumeroCartaoCredito getNumero();
    CvvCartaoCredito getCvv();
    VencimentoCartaoCredito getVencimento();
}
