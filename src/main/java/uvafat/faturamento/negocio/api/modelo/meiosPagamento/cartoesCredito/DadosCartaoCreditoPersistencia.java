package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;

import java.util.Objects;

public class DadosCartaoCreditoPersistencia implements DadosCartaoCredito {

    public static class Numero implements NumeroCartaoCredito {

        private String numero;

        Numero(String numero) {
            Objects.requireNonNull(numero, "Número não pode ser nulo.");

            // Remove espaços (início e fim) e pontuações
            numero = numero.trim().replace("[.]", "").toUpperCase();

            if (!numero.matches("[0-9]{16}")) {
                throw new ArgumentoInvalido("Número de cartão inválido para persistência.");
            }

            this.numero = numero;
        }

        public String getValor() {
            return this.numero;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Numero numero1 = (Numero) o;
            return NumeroCartaoCredito.paraExibicao(this).equals(NumeroCartaoCredito.paraExibicao(numero1));
        }

        @Override
        public int hashCode() {
            return NumeroCartaoCredito.paraExibicao(this).hashCode();
        }

        @Override
        public String toString() {
            return NumeroCartaoCredito.paraExibicao(this).toString();
        }
    }

    public static class Cvv implements CvvCartaoCredito {

        private String cvv = null;

        Cvv(String cvv) {
            Objects.requireNonNull(cvv, "CVV não pode ser nulo.");

            if (!cvv.matches("\\d{3}")) {
                throw new ArgumentoInvalido("Formato de CVV inválido.");
            }

            this.cvv = cvv;
        }

        public String getValor() {
            return this.cvv;
        }
    }

    private Numero numero;
    private Cvv cvv;
    private VencimentoCartaoCredito vencimento;

    DadosCartaoCreditoPersistencia(Numero numero, Cvv cvv, VencimentoCartaoCredito vencimento) {
        Objects.requireNonNull(numero, "Número do cartão de crédito não pode ser nulo.");
        Objects.requireNonNull(cvv, "CVV do cartão de crédito não pode ser nulo.");
        Objects.requireNonNull(vencimento, "Vencimento do cartão de crédito não pode ser nulo.");
        this.numero = numero;
        this.cvv = cvv;
        this.vencimento = vencimento;
    }

    @Override
    public boolean isOculto() {
        return false;
    }

    @Override
    public Numero getNumero() {
        return numero;
    }

    @Override
    public Cvv getCvv() {
        return cvv;
    }

    @Override
    public VencimentoCartaoCredito getVencimento() {
        return vencimento;
    }
}
