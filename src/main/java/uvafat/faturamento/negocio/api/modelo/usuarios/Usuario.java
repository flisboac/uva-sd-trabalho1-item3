package uvafat.faturamento.negocio.api.modelo.usuarios;

import uvafat.faturamento.negocio.api.suporte.entidades.Entidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidadePadrao;

public interface Usuario extends Entidade {

    class Id extends IdEntidadePadrao<Long> implements IdEntidade {
        private Id() {}
        public Id(Long id) { super(id); }
        public static final Id NULO = new Id();
    }

    // Propriedades
    Id getId();
    String getNome();
}
