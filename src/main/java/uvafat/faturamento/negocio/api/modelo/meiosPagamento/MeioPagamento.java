package uvafat.faturamento.negocio.api.modelo.meiosPagamento;

import uvafat.faturamento.negocio.api.suporte.entidades.Entidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidade;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidadePadrao;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.DadosCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;

public interface MeioPagamento extends Entidade {

    class Id extends IdEntidadePadrao<Long> implements IdEntidade {
        private Id() {}
        public Id(Long id) { super(id); }
        public static final Id NULO = new Id();
    }

    // Propriedades
    Id getId();
    Usuario getUsuario();
    DadosCartaoCredito getDadosCartaoCredito();
}
