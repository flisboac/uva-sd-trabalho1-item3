package uvafat.faturamento.negocio.api.modelo.meiosPagamento;

import uvafat.faturamento.negocio.api.suporte.entidades.EntidadePadrao;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.DadosCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.suporte.excecoes.EstadoInvalido;

import java.util.Objects;

public class DadosMeioPagamento extends EntidadePadrao<MeioPagamento.Id> implements MeioPagamento {

    private Usuario usuario;
    private DadosCartaoCredito dadosCartaoCredito;

    public DadosMeioPagamento(Id id) {
        super(id);
    }

    public DadosMeioPagamento(Usuario usuario, DadosCartaoCredito dadosCartaoCredito) {
        super(Id.NULO);

        Objects.requireNonNull(usuario);
        Objects.requireNonNull(dadosCartaoCredito);

        this.usuario = usuario;
        this.dadosCartaoCredito = dadosCartaoCredito;

        validarEstado();
    }

    public DadosMeioPagamento(MeioPagamento entidade) {
        super(entidade.getId());
        this.usuario = entidade.getUsuario();
        this.dadosCartaoCredito = entidade.getDadosCartaoCredito();
    }

    private void validarEstado() {

        if (this.usuario.getId().isNulo()) {
            throw new EstadoInvalido("Usuário não pode ser transiente.");
        }
    }

    @Override
    public Usuario getUsuario() {
        return usuario;
    }

    @Override
    public DadosCartaoCredito getDadosCartaoCredito() {
        return dadosCartaoCredito;
    }
}
