package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;

import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.Objects;

public class VencimentoCartaoCredito {

    private ZonedDateTime dataHoraLimite;

    private VencimentoCartaoCredito(Integer mes, Integer ano) {
        Objects.requireNonNull(mes, "Mês do vencimento não pode ser vazio.");
        Objects.requireNonNull(ano, "Ano do vencimento não pode ser vazio.");

        if (mes < 1 && mes > 12) {
            throw new ArgumentoInvalido("Mês do vecimento fora dos limites permitidos.");
        }

        if (ano < 1900 && mes > 9999) {
            throw new ArgumentoInvalido("Ano do vecimento fora dos limites permitidos.");
        }

        this.dataHoraLimite = LocalDate.of(ano, Month.of(mes), 1)
            .with(TemporalAdjusters.lastDayOfMonth())
            .atTime(LocalTime.MAX)
            .atZone(ZoneOffset.UTC);
    }

    public static VencimentoCartaoCredito em(Month mes, int ano) {
        return new VencimentoCartaoCredito(mes.getValue(), ano);
    }

    public static VencimentoCartaoCredito em(int mes, int ano) {
        return new VencimentoCartaoCredito(mes, ano);
    }

    public Month getMes() {
        return getValor().getMonth();
    }

    public Integer getAno() {
        return getValor().getYear();
    }

    public LocalDate getValor() {
        return this.dataHoraLimite.toLocalDate();
    }

    public ZonedDateTime getDataHoraLimite() {
        return this.dataHoraLimite;
    }
}
