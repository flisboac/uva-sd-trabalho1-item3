package uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito;

public interface CvvCartaoCredito {

    String getValor();

    static DadosCartaoCreditoPersistencia.Cvv paraPersistencia(String numero) {
        return new DadosCartaoCreditoPersistencia.Cvv(numero);
    }

    static DadosCartaoCreditoPersistencia.Cvv paraPersistencia(NumeroCartaoCredito numero) {
        return paraPersistencia(numero.getValor());
    }

    static DadosCartaoCreditoExibicao.Cvv paraExibicao(String numero) {
        return DadosCartaoCreditoExibicao.Cvv.INSTANCIA;
    }

    static DadosCartaoCreditoExibicao.Cvv paraExibicao(NumeroCartaoCredito numero) {
        return paraExibicao(numero.getValor());
    }
}
