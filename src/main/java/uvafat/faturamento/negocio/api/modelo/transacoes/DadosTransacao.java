package uvafat.faturamento.negocio.api.modelo.transacoes;

import uvafat.faturamento.negocio.api.suporte.entidades.EntidadePadrao;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;
import uvafat.faturamento.negocio.api.suporte.excecoes.EstadoInvalido;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

public class DadosTransacao extends EntidadePadrao<Transacao.Id> implements Transacao {

    private MeioPagamento meioPagamento;
    private Estado estado;
    private BigDecimal valor;
    private ZonedDateTime dataInicio;
    private ZonedDateTime dataTermino;

    public DadosTransacao(Id id) {
        super(id);
    }

    public DadosTransacao(MeioPagamento meioPagamento, BigDecimal valor, ZonedDateTime dataInicio) {
        super(Id.NULO);

        Objects.requireNonNull(meioPagamento);
        Objects.requireNonNull(valor);
        Objects.requireNonNull(dataInicio);

        this.meioPagamento = meioPagamento;
        this.valor = valor;
        this.dataInicio = dataInicio;
        this.estado = Estado.INICIADA;

        validarEstado();
    }

    // @LoadAll
    public DadosTransacao(Id id, MeioPagamento meioPagamento, Estado estado, BigDecimal valor, ZonedDateTime dataInicio, ZonedDateTime dataTermino) {
        super(id);

        Objects.requireNonNull(meioPagamento);
        Objects.requireNonNull(estado);
        Objects.requireNonNull(valor);
        Objects.requireNonNull(dataInicio);
        //Objects.requireNonNull(dataTermino);

        this.meioPagamento = meioPagamento;
        this.estado = estado;
        this.valor = valor;
        this.dataInicio = dataInicio;
        this.dataTermino = dataTermino;

        validarEstado();
    }

    public DadosTransacao(Transacao entidade) {
        super(entidade.getId());
        this.meioPagamento = entidade.getMeioPagamento();
        this.estado = entidade.getEstado();
        this.valor = entidade.getValor();
        this.dataInicio = entidade.getDataInicio();
        this.dataTermino = entidade.getDataTermino().orElse(null);
    }

    @Override
    public MeioPagamento getMeioPagamento() {
        return meioPagamento;
    }

    @Override
    public Estado getEstado() {
        return estado;
    }

    @Override
    public BigDecimal getValor() {
        return valor;
    }

    @Override
    public ZonedDateTime getDataInicio() {
        return dataInicio;
    }

    @Override
    public Optional<ZonedDateTime> getDataTermino() {
        if (estado.isTerminal()) Objects.requireNonNull(dataTermino); // Just to be sure.
        return Optional.ofNullable(dataTermino);
    }

    public DadosTransacao terminar(Estado estado, ZonedDateTime dataTermino) {
        Objects.requireNonNull(estado);
        Objects.requireNonNull(dataTermino);

        if (this.estado.isTerminal()) {
            throw new EstadoInvalido("Transação já se encontra finalizada.");
        }

        if (!estado.isTerminal()) {
            throw new ArgumentoInvalido("O término de uma transação só pode ser efetuado ao passar um estado terminal.");
        }

        if (dataTermino.compareTo(this.dataInicio) <= 0) {
            throw new ArgumentoInvalido("Data de término da transação inválida..");
        }

        this.estado = estado;
        this.dataTermino = dataTermino;

        // Deveria retornar uma nova entidade.
        return this;
    }

    private void validarEstado() {

        if (meioPagamento.getId().isNulo()) {
            throw new ArgumentoInvalido("Meio de pagamento transiente.");
        }

        if (this.estado.isTerminal()) {

            if (this.dataTermino == null) {
                throw new ArgumentoInvalido("Data de término deve ser fornecida quando o estado da transação for terminal.");
            }

            if (this.dataTermino.compareTo(this.dataInicio) < 0) {
                throw new ArgumentoInvalido("Data de início deve ser menor ou igual à data de término");
            }
        }
    }
}
