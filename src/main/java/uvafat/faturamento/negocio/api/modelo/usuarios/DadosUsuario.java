package uvafat.faturamento.negocio.api.modelo.usuarios;

import uvafat.faturamento.negocio.api.suporte.entidades.EntidadePadrao;
import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;

import java.util.Objects;

public class DadosUsuario extends EntidadePadrao<Usuario.Id> implements Usuario {

    private String nome;

    public DadosUsuario(Id id) {
        super(id);
    }

    public DadosUsuario(String nome) {
        super(Id.NULO);

        Objects.requireNonNull(nome);

        this.nome = nome;

        validarEstado();
    }

    public DadosUsuario(Usuario entidade) {
        this(entidade.getId());
        this.nome = entidade.getNome();
    }

    public DadosUsuario(Id idUsuario, String nomeUsuario) {
        super(idUsuario);

        Objects.requireNonNull(nomeUsuario);

        this.nome = nomeUsuario;

        validarEstado();
    }

    public void validarEstado() {

        if (this.nome.length() > 256) {
            throw new ArgumentoInvalido("Tamanho do nome do usuário não pode exceder 256 caracteres.");
        }
    }

    @Override
    public String getNome() {
        return this.nome;
    }
}
