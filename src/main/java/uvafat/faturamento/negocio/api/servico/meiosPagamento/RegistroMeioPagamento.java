package uvafat.faturamento.negocio.api.servico.meiosPagamento;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.DadosCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.usuarios.RegistroUsuario;
import uvafat.faturamento.negocio.api.servico.usuarios.RepositorioUsuario;
import uvafat.faturamento.negocio.api.suporte.entidades.EntidadeRegistro;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;

import java.util.Objects;

public class RegistroMeioPagamento implements MeioPagamento, EntidadeRegistro<MeioPagamento.Id, MeioPagamento> {

    private MeioPagamento dadosEntidade;

    private RepositorioMeioPagamento repositorioMeioPagamento;
    private RepositorioUsuario repositorioUsuario;

    public RegistroMeioPagamento(MeioPagamento dadosEntidade,
            RepositorioMeioPagamento repositorioMeioPagamento,
            RepositorioUsuario repositorioUsuario) {
        this(repositorioMeioPagamento, repositorioUsuario);

        Objects.requireNonNull(dadosEntidade);
        assert !dadosEntidade.getId().isNulo();

        this.dadosEntidade = dadosEntidade;
    }

    public RegistroMeioPagamento(RepositorioMeioPagamento repositorioMeioPagamento,
            RepositorioUsuario repositorioUsuario) {
        this.repositorioMeioPagamento = repositorioMeioPagamento;
        this.repositorioUsuario = repositorioUsuario;
    }

    @Override
    public MeioPagamento getDadosEntidade() {
        return dadosEntidade;
    }

    @Override
    public Id getId() {
        return dadosEntidade.getId();
    }

    @Override
    public RegistroUsuario getUsuario() {
        return repositorioUsuario.buscar(dadosEntidade.getUsuario()).get();
    }

    @Override
    public DadosCartaoCredito getDadosCartaoCredito() {
        return dadosEntidade.getDadosCartaoCredito();
    }
}
