package uvafat.faturamento.negocio.api.servico.transacoes;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.modelo.transacoes.DadosTransacao;
import uvafat.faturamento.negocio.api.suporte.entidades.EntidadeRegistro;
import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

public class RegistroTransacao implements Transacao, EntidadeRegistro<Transacao.Id, Transacao> {

    private DadosTransacao dadosTransacao;

    private RepositorioTransacao repositorioTransacao;

    public RegistroTransacao(DadosTransacao dadosTransacao, RepositorioTransacao repositorioTransacao) {
        this(repositorioTransacao);

        Objects.requireNonNull(dadosTransacao);
        assert !dadosTransacao.getId().isNulo();

        this.dadosTransacao = dadosTransacao;
    }

    public RegistroTransacao(RepositorioTransacao repositorioTransacao) {
        Objects.requireNonNull(repositorioTransacao);

        this.repositorioTransacao = repositorioTransacao;
    }

    public RegistroTransacao finalizar(Estado estadoFinal) {
        dadosTransacao = dadosTransacao.terminar(estadoFinal, ZonedDateTime.now(ZoneOffset.UTC));
        return repositorioTransacao.salvar(dadosTransacao);
    }

    @Override
    public Transacao getDadosEntidade() {
        return dadosTransacao;
    }

    @Override
    public Id getId() {
        return dadosTransacao.getId();
    }

    @Override
    public MeioPagamento getMeioPagamento() {
        return dadosTransacao.getMeioPagamento();
    }

    @Override
    public Estado getEstado() {
        return dadosTransacao.getEstado();
    }

    @Override
    public BigDecimal getValor() {
        return dadosTransacao.getValor();
    }

    @Override
    public ZonedDateTime getDataInicio() {
        return dadosTransacao.getDataInicio();
    }

    @Override
    public Optional<ZonedDateTime> getDataTermino() {
        return dadosTransacao.getDataTermino();
    }
}
