package uvafat.faturamento.negocio.api.servico.transacoes;

import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.suporte.repositorios.Repositorio;

import java.util.List;

public interface RepositorioTransacao extends Repositorio<Transacao.Id, Transacao, RegistroTransacao> {

    List<Transacao> listarPorUsuario(Usuario usuario);
}
