package uvafat.faturamento.negocio.api.servico.usuarios;

import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.suporte.repositorios.Repositorio;

public interface RepositorioUsuario extends Repositorio<Usuario.Id, Usuario, RegistroUsuario> {

    // Métodos de persistência
    RegistroUsuario criar(String nome);
}
