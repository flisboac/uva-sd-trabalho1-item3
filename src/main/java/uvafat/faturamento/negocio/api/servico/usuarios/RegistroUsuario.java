package uvafat.faturamento.negocio.api.servico.usuarios;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.DadosMeioPagamento;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.cartoesCredito.DadosCartaoCredito;
import uvafat.faturamento.negocio.api.modelo.transacoes.DadosTransacao;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RegistroMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RegistroTransacao;
import uvafat.faturamento.negocio.api.suporte.entidades.EntidadeRegistro;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidade;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;
import uvafat.faturamento.negocio.api.modelo.usuarios.DadosUsuario;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RepositorioMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RepositorioTransacao;
import uvafat.faturamento.negocio.api.suporte.excecoes.EstadoInvalido;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class RegistroUsuario implements Usuario, EntidadeRegistro<Usuario.Id, Usuario> {

    // Propriedades internas
    private DadosUsuario dadosEntidade;

    // Propriedades injetáveis
    private RepositorioUsuario repositorioUsuario;
    private RepositorioTransacao repositorioTransacao;
    private RepositorioMeioPagamento repositorioMeioPagamento;

    public RegistroUsuario(DadosUsuario dadosEntidade,
            RepositorioUsuario repositorioUsuario,
            RepositorioTransacao repositorioTransacao,
            RepositorioMeioPagamento repositorioMeioPagamento) {
        this(repositorioUsuario, repositorioTransacao, repositorioMeioPagamento);

        Objects.requireNonNull(dadosEntidade);
        assert !dadosEntidade.getId().isNulo();

        this.dadosEntidade = dadosEntidade;
    }

    public RegistroUsuario(RepositorioUsuario repositorioUsuario,
            RepositorioTransacao repositorioTransacao,
            RepositorioMeioPagamento repositorioMeioPagamento) {
        Objects.requireNonNull(repositorioUsuario);
        Objects.requireNonNull(repositorioTransacao);
        Objects.requireNonNull(repositorioMeioPagamento);

        this.repositorioUsuario = repositorioUsuario;
        this.repositorioTransacao = repositorioTransacao;
        this.repositorioMeioPagamento = repositorioMeioPagamento;
    }

    public List<Transacao> getTransacoes() {
        return repositorioTransacao.listarPorUsuario(this);
    }

    public List<MeioPagamento> getMeiosPagamento() {
        return repositorioMeioPagamento.listarPorUsuario(this);
    }

    public RegistroMeioPagamento addMeioPagamento(DadosCartaoCredito dadosCartaoCredito) {
        DadosMeioPagamento novoMeioPagamento = new DadosMeioPagamento(this, dadosCartaoCredito);
        RegistroMeioPagamento novoRegistro = repositorioMeioPagamento.salvar(novoMeioPagamento);
        return novoRegistro;
    }

    public RegistroTransacao iniciarTransacao(MeioPagamento meioPagamento, BigDecimal valor) {
        if (!meioPagamento.getUsuario().getId().equals(meioPagamento.getUsuario().getId())) {
            throw new EstadoInvalido("Meio de pagamento não pertence ao usuário.");
        }
        DadosTransacao novaTransacao = new DadosTransacao(meioPagamento, valor, ZonedDateTime.now(ZoneOffset.UTC));
        RegistroTransacao novoRegistro = repositorioTransacao.salvar(novaTransacao);
        return novoRegistro;
    }

    public int getQtdMeiosPagamento() {
        // TODO Contagem mais eficiente
        return getMeiosPagamento().size();
    }

    public int getQtdTransacoes() {
        // TODO Contagem mais eficiente
        return getTransacoes().size();
    }

    // ---

    @Override
    public Usuario.Id getId() {
        return dadosEntidade.getId();
    }

    @Override
    public Usuario getDadosEntidade() {
        return dadosEntidade;
    }

    @Override
    public String getNome() {
        return dadosEntidade.getNome();
    }

}
