package uvafat.faturamento.negocio.api.servico.meiosPagamento;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.suporte.repositorios.Repositorio;

import java.util.List;

public interface RepositorioMeioPagamento extends Repositorio<MeioPagamento.Id, MeioPagamento, RegistroMeioPagamento> {

    List<MeioPagamento> listarPorUsuario(Usuario usuario);
}
