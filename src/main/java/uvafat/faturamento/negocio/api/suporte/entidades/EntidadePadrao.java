package uvafat.faturamento.negocio.api.suporte.entidades;

import uvafat.faturamento.negocio.api.suporte.excecoes.EstadoInvalido;

public abstract class EntidadePadrao<Id extends IdEntidade> implements Entidade {

    private Id id;

    public EntidadePadrao(Id id) {
        assert id != null;
        this.id = id;
    }

    @Override
    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        if (this.id.isNulo()) {
            this.id = id;
        } else {
            throw new EstadoInvalido("Entidade já está identificada!");
        }
    }
}
