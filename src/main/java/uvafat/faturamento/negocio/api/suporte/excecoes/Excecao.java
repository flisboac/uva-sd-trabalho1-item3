package uvafat.faturamento.negocio.api.suporte.excecoes;

import uvafat.faturamento.negocio.api.suporte.IdErro;

public class Excecao extends RuntimeException {

    private final IdErro idErro;

    public Excecao() {
        super(IdErro.ERRO.getMensagemPadrao());
        this.idErro = IdErro.ERRO;
    }

    public Excecao(String s) {
        super(s);
        this.idErro = IdErro.ERRO;
    }

    public Excecao(String s, Throwable throwable) {
        super(s, throwable);
        this.idErro = IdErro.ERRO;
    }

    public Excecao(Throwable throwable) {
        super(throwable);
        this.idErro = IdErro.ERRO;
    }

    public Excecao(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
        this.idErro = IdErro.ERRO;
    }

    public Excecao(IdErro idErro) {
        super(idErro.getMensagemPadrao());
        this.idErro = idErro;
    }

    public Excecao(IdErro idErro, String mensagemErro) {
        super(mensagemErro);
        this.idErro = idErro;
    }

    public IdErro getIdErro() {
        return idErro;
    }
}
