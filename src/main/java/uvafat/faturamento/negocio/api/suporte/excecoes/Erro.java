package uvafat.faturamento.negocio.api.suporte.excecoes;

import uvafat.faturamento.negocio.api.suporte.IdErro;

public class Erro extends Exception {

    private final IdErro idErro;

    public Erro() {
        super(IdErro.ERRO.getMensagemPadrao());
        this.idErro = IdErro.ERRO;
    }

    public Erro(String s) {
        super(s);
        this.idErro = IdErro.ERRO;
    }

    public Erro(String s, Throwable throwable) {
        super(s, throwable);
        this.idErro = IdErro.ERRO;
    }

    public Erro(Throwable throwable) {
        super(throwable);
        this.idErro = IdErro.ERRO;
    }

    public Erro(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
        this.idErro = IdErro.ERRO;
    }

    public Erro(IdErro idErro) {
        super(idErro.getMensagemPadrao());
        this.idErro = idErro;
    }

    public Erro(IdErro idErro, String mensagemErro) {
        super(mensagemErro);
        this.idErro = idErro;
    }

    public IdErro getIdErro() {
        return idErro;
    }
}
