package uvafat.faturamento.negocio.api.suporte.entidades;

public interface EntidadeRegistro<Id extends IdEntidade, T extends Entidade> extends Entidade {

    T getDadosEntidade();
}
