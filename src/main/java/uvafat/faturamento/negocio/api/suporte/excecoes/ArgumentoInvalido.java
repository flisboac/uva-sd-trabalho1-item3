package uvafat.faturamento.negocio.api.suporte.excecoes;

import uvafat.faturamento.negocio.api.suporte.IdErro;

public class ArgumentoInvalido extends Excecao {

    public ArgumentoInvalido() {
    }

    public ArgumentoInvalido(String s) {
        super(s);
    }

    public ArgumentoInvalido(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ArgumentoInvalido(Throwable throwable) {
        super(throwable);
    }

    public ArgumentoInvalido(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public ArgumentoInvalido(IdErro idErro) {
        super(idErro);
    }

    public ArgumentoInvalido(IdErro idErro, String mensagemErro) {
        super(idErro, mensagemErro);
    }
}
