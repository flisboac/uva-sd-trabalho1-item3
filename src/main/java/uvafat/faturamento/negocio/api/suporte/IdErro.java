package uvafat.faturamento.negocio.api.suporte;

public enum IdErro {

    OK(0, "Operação bem sucedida."),
    ERRO(1, "Operação falhou.");

    private final int id;
    private final String mensagemPadrao;

    IdErro(int id, String mensagemPadrao) {
        this.id = id;
        this.mensagemPadrao = mensagemPadrao;
    }

    public int getId() {
        return id;
    }

    public String getMensagemPadrao() {
        return mensagemPadrao;
    }
}
