package uvafat.faturamento.negocio.api.suporte.repositorios;

import uvafat.faturamento.negocio.api.suporte.entidades.Entidade;
import uvafat.faturamento.negocio.api.suporte.entidades.EntidadeRegistro;
import uvafat.faturamento.negocio.api.suporte.entidades.IdEntidade;

import java.util.List;
import java.util.Optional;

public interface Repositorio<TipoId extends IdEntidade, TipoEntidade extends Entidade, TipoRegistro extends EntidadeRegistro<TipoId, TipoEntidade>> {

    // Métodos de busca
    Optional<TipoRegistro> buscar(TipoEntidade entidade); // ID ou nome

    // Métodos de persistência
    TipoRegistro salvar(TipoEntidade entidade);
}
