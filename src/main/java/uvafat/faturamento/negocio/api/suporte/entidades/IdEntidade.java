package uvafat.faturamento.negocio.api.suporte.entidades;

import java.io.Serializable;

public interface IdEntidade extends Serializable {
    boolean isNulo();
}
