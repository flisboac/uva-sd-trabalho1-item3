package uvafat.faturamento.negocio.api.suporte.excecoes;

import uvafat.faturamento.negocio.api.suporte.IdErro;

public class EstadoInvalido extends Excecao {

    public EstadoInvalido() {
    }

    public EstadoInvalido(String s) {
        super(s);
    }

    public EstadoInvalido(String s, Throwable throwable) {
        super(s, throwable);
    }

    public EstadoInvalido(Throwable throwable) {
        super(throwable);
    }

    public EstadoInvalido(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public EstadoInvalido(IdErro idErro) {
        super(idErro);
    }

    public EstadoInvalido(IdErro idErro, String mensagemErro) {
        super(idErro, mensagemErro);
    }
}
