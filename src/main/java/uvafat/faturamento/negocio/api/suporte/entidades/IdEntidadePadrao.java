package uvafat.faturamento.negocio.api.suporte.entidades;

import java.io.Serializable;
import java.util.Objects;

public abstract class IdEntidadePadrao<T extends Serializable> implements IdEntidade {

    private T id;

    public IdEntidadePadrao() {}

    public IdEntidadePadrao(T id) {
        this.id = id;
    }

    @Override
    public boolean isNulo() {
        return id == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdEntidadePadrao<?> that = (IdEntidadePadrao<?>) o;
        return Objects.equals(id, that.id);
    }

    public T getValor() {
        return id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
