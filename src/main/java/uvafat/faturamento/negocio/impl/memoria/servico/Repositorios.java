package uvafat.faturamento.negocio.impl.memoria.servico;

import uvafat.faturamento.negocio.api.servico.meiosPagamento.RepositorioMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RepositorioTransacao;
import uvafat.faturamento.negocio.api.suporte.excecoes.ArgumentoInvalido;
import uvafat.faturamento.negocio.impl.memoria.infra.detail.DaoGenericoMemoria;
import uvafat.faturamento.negocio.api.servico.usuarios.RepositorioUsuario;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class Repositorios {

    private static DaoGenericoMemoria dao;
    private static Map<Class<?>, Object> instancias = new HashMap<>();
    private static final Map<Class<?>, Class<?>> implementacoes;

    static {
        Map<Class<?>, Class<?>> implementacoes_ = new HashMap<>();
        implementacoes_.put(RepositorioUsuario.class, RepositorioUsuarioImpl.class);
        implementacoes_.put(RepositorioMeioPagamento.class, RepositorioMeioPagamentoImpl.class);
        implementacoes_.put(RepositorioTransacao.class, RepositorioTransacaoImpl.class);
        implementacoes = Collections.unmodifiableMap(implementacoes_);
    }

    private static DaoGenericoMemoria getDao() {
        if (dao == null) {
            dao = new DaoGenericoMemoria();
        }
        return dao;
    }

    public static <T> T para(Class<T> classeRepositorio) {
        T instancia = classeRepositorio.cast(instancias.get(classeRepositorio));
        if (instancia != null) return instancia;

        Class<?> classeImplementacao = implementacoes.get(classeRepositorio);
        if (classeImplementacao == null) {
            throw new ArgumentoInvalido("Implementação para a interface de repositório desconhecida.");
        }

        Constructor<?> construtor = null;
        try {
            construtor = classeImplementacao.getConstructor(DaoGenericoMemoria.class);
            Object o = construtor.newInstance(getDao());
            instancias.put(classeRepositorio, o);
            instancia = classeRepositorio.cast(o);

        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException("Erro ao instanciar repositório para a interface '" + classeRepositorio.getName() + "'.", e);
        }

        return instancia;
    }
}
