package uvafat.faturamento.negocio.impl.memoria.infra.detail;

import uvafat.faturamento.negocio.api.suporte.entidades.Entidade;
import uvafat.faturamento.negocio.api.suporte.excecoes.EstadoInvalido;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

// Simplório, mas o tempo urge! O certo seria usar um banco de dados! Não julgue, pq tá tudo errado nesse pacote de
// implementação!
public class DaoGenericoMemoria {

    private static DaoGenericoMemoria instancia;

    public static DaoGenericoMemoria getInstance() {
        if (instancia == null) instancia = new DaoGenericoMemoria();
        return instancia;
    }

    private Map<Class<? extends Entidade>, Class<? extends Entidade>> implementacoes = new HashMap<>();
    private Map<Class<? extends Entidade>, Long> sequencias = new HashMap<>();
    private Map<Class<? extends Entidade>, List<? extends Entidade>> objetos = new HashMap<>();

    public <I extends Entidade, C extends I> void registrarImplementacao(Class<I> classeInterface, Class<C> classeDados) {
        assert classeInterface.isAssignableFrom(classeDados);
        implementacoes.put(classeInterface, classeDados);
        implementacoes.put(classeDados, classeDados);
    }

    private <T extends Entidade> Class<T> getClasseEntidade(T entidade) {
        Class<T> classeDados = (Class<T>) entidade.getClass();
        return getClasseEntidade(classeDados);
    }

    private <T extends Entidade> Class<T> getClasseEntidade(Class<T> classeInterface) {
        Class<T> classeDados = (Class<T>) implementacoes.get(classeInterface);
        Objects.requireNonNull(classeDados, "Classe de dados não registrada para a interface '" + classeInterface.getName() + "'.");
        return classeDados;
    }

    private <T extends Entidade> Long proximoId(Class<T> classeEntidade) {
        Long proximoId = sequencias.get(classeEntidade);

        if (proximoId == null) {
            proximoId = 1l;
        }

        sequencias.put(classeEntidade, proximoId + 1l);
        return proximoId;
    }

    private <T extends Entidade> Optional<Integer> buscarIndice(Class<T> classeEntidade, Predicate<T> mapeador) {
        List<T> entidades = getEntidades(classeEntidade);
        for (int i = 0; i < entidades.size(); ++i) {
            T entidade = entidades.get(i);

            if (mapeador.test(entidade)) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    private <T extends Entidade> Optional<Integer> buscarIndice(T entidade) {
        Class<T> classeEntidade = getClasseEntidade(entidade);
        if (!entidade.getId().isNulo()) {
            return buscarIndice(classeEntidade, entidadePersistida -> entidade.getId().equals(entidadePersistida.getId()));
        }
        return Optional.empty();
    }

    private <T extends Entidade> Optional<T> obterIndice(Class<T> classeEntidade, Optional<Integer> indice) {
        if (indice.isPresent()) {
            List<T> entidades = getEntidades(classeEntidade);
            return Optional.of(entidades.get(indice.get()));
        }
        return Optional.empty();
    }

    public <T extends Entidade> List<T> listar(Class<T> classeEntidade) {
        List<T> entidades = getEntidades(classeEntidade);
        if (entidades != null) {
            return new ArrayList<>(entidades);
        }
        return Collections.emptyList();
    }

    public <T extends Entidade> List<T> listar(Class<T> classeEntidade, Predicate<T> mapeador) {
        List<T> entidades = listar(classeEntidade);
        return entidades.stream().filter(mapeador).collect(Collectors.toList());
    }

    public <T extends Entidade> Optional<T> buscar(T entidade) {
        Class<T> classeEntidade = getClasseEntidade(entidade);
        Optional<Integer> indice = buscarIndice(entidade);
        return obterIndice(classeEntidade, indice);
    }

    public <T extends Entidade> Optional<T> buscar(Class<T> classeEntidade, Predicate<T> mapeador) {
        Optional<Integer> indice = buscarIndice(classeEntidade, mapeador);
        return obterIndice(classeEntidade, indice);
    }

    public <T extends Entidade> Optional<Long> salvar(T entidade) {
        Class<T> classeEntidade = getClasseEntidade(entidade);
        List<T> entidades = getEntidades(classeEntidade);

        if (entidade.getId().isNulo()) {
            // insert
            entidades.add(entidade);
            return Optional.of(proximoId(classeEntidade));

        } else {
            Optional<Integer> indiceEntidade = buscarIndice(entidade);

            if (indiceEntidade.isPresent()) {
                // update
                entidades.set(indiceEntidade.get(), entidade);

            } else {
                // ERRO!
                throw new EstadoInvalido("Entidade com o ID especificado não existe!");
            }
        }

        // Retorno apenas quando a entidade for criada (insert)
        return Optional.empty();
    }

    private <T extends Entidade> List<T> getEntidades(Class<T> classeEntidade) {
        Class<T> classeDados = getClasseEntidade(classeEntidade);
        List<T> entidades = (List<T>) objetos.get(classeDados);
        if (entidades == null) {
            entidades = new ArrayList<>();
            objetos.put(classeDados, entidades);
        }
        return entidades;
    }
}
