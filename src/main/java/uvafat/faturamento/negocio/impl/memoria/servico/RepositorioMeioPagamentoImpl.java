package uvafat.faturamento.negocio.impl.memoria.servico;

import uvafat.faturamento.negocio.api.modelo.meiosPagamento.DadosMeioPagamento;
import uvafat.faturamento.negocio.api.modelo.meiosPagamento.MeioPagamento;
import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RegistroMeioPagamento;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RepositorioMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RegistroTransacao;
import uvafat.faturamento.negocio.api.servico.usuarios.RepositorioUsuario;
import uvafat.faturamento.negocio.impl.memoria.infra.detail.DaoGenericoMemoria;

import java.util.List;
import java.util.Optional;

public class RepositorioMeioPagamentoImpl implements RepositorioMeioPagamento {

    private DaoGenericoMemoria dao;

    public RepositorioMeioPagamentoImpl(DaoGenericoMemoria dao) {
        this.dao = dao;
        this.dao.registrarImplementacao(MeioPagamento.class, DadosMeioPagamento.class);
    }

    @Override
    public Optional<RegistroMeioPagamento> buscar(MeioPagamento entidade) {
        final MeioPagamento entidadePesquisa = entidade instanceof RegistroMeioPagamento ? RegistroMeioPagamento.class.cast(entidade).getDadosEntidade() : entidade;
        Optional<MeioPagamento> entidadeEncontrada = dao.buscar(entidadePesquisa);

        if (entidadeEncontrada.isPresent()) {
            DadosMeioPagamento dados = DadosMeioPagamento.class.cast(entidadeEncontrada.get());
            return Optional.of(instanciarRegistro(dados));
        }

        return Optional.empty();
    }

    @Override
    public RegistroMeioPagamento salvar(MeioPagamento entidade) {
        DadosMeioPagamento dados = entidade instanceof DadosMeioPagamento
            ? DadosMeioPagamento.class.cast(entidade)
            : new DadosMeioPagamento(entidade);
        return doSalvar(dados);
    }

    @Override
    public List<MeioPagamento> listarPorUsuario(Usuario usuario) {
        return dao.listar(MeioPagamento.class,
            meioPagamento -> !usuario.getId().isNulo()
                ? meioPagamento.getUsuario().getId().equals(usuario.getId())
                : meioPagamento.getUsuario().equals(usuario.getNome()));
    }

    private RegistroMeioPagamento doSalvar(DadosMeioPagamento dados) {
        Optional<Long> novoId = dao.salvar(dados);

        if (novoId.isPresent()) {
            MeioPagamento.Id id = new MeioPagamento.Id(novoId.get());
            dados.setId(id);
        }

        return instanciarRegistro(dados);
    }

    private RegistroMeioPagamento instanciarRegistro(DadosMeioPagamento dados) {
        return new RegistroMeioPagamento(
            dados,
            this,
            Repositorios.para(RepositorioUsuario.class)
        );
    }
}
