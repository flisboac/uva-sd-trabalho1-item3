package uvafat.faturamento.negocio.impl.memoria.servico;

import uvafat.faturamento.negocio.api.modelo.transacoes.DadosTransacao;
import uvafat.faturamento.negocio.api.modelo.transacoes.Transacao;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.transacoes.RegistroTransacao;
import uvafat.faturamento.negocio.api.servico.transacoes.RepositorioTransacao;
import uvafat.faturamento.negocio.api.servico.usuarios.RegistroUsuario;
import uvafat.faturamento.negocio.impl.memoria.infra.detail.DaoGenericoMemoria;

import java.util.List;
import java.util.Optional;

public class RepositorioTransacaoImpl implements RepositorioTransacao {

    private DaoGenericoMemoria dao;

    public RepositorioTransacaoImpl(DaoGenericoMemoria dao) {
        this.dao = dao;
        this.dao.registrarImplementacao(Transacao.class, DadosTransacao.class);
    }

    @Override
    public Optional<RegistroTransacao> buscar(Transacao entidade) {
        final Transacao entidadePesquisa = entidade instanceof RegistroTransacao ? RegistroTransacao.class.cast(entidade).getDadosEntidade() : entidade;
        Optional<Transacao> entidadeEncontrada = dao.buscar(entidadePesquisa);

        if (entidadeEncontrada.isPresent()) {
            DadosTransacao dados = DadosTransacao.class.cast(entidadeEncontrada.get());
            return Optional.of(instanciarRegistro(dados));
        }

        return Optional.empty();
    }

    @Override
    public RegistroTransacao salvar(Transacao entidade) {
        DadosTransacao dados = entidade instanceof DadosTransacao
            ? DadosTransacao.class.cast(entidade)
            : new DadosTransacao(entidade);
        return doSalvar(dados);
    }

    private RegistroTransacao doSalvar(DadosTransacao dados) {
        Optional<Long> novoId = dao.salvar(dados);

        if (novoId.isPresent()) {
            Transacao.Id id = new Transacao.Id(novoId.get());
            dados.setId(id);
        }

        return instanciarRegistro(dados);
    }

    private RegistroTransacao instanciarRegistro(DadosTransacao dados) {
        return new RegistroTransacao(
            dados,
            this
        );
    }

    @Override
    public List<Transacao> listarPorUsuario(Usuario usuario) {
        return dao.listar(Transacao.class,
            transacao -> !usuario.getId().isNulo()
                ? transacao.getMeioPagamento().getUsuario().getId().equals(usuario.getId())
                : transacao.getMeioPagamento().getUsuario().equals(usuario.getNome()));
    }
}
