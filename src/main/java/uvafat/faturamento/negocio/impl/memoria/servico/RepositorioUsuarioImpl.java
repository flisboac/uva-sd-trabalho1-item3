package uvafat.faturamento.negocio.impl.memoria.servico;

import uvafat.faturamento.negocio.api.modelo.usuarios.DadosUsuario;
import uvafat.faturamento.negocio.api.modelo.usuarios.Usuario;
import uvafat.faturamento.negocio.api.servico.meiosPagamento.RepositorioMeioPagamento;
import uvafat.faturamento.negocio.api.servico.transacoes.RepositorioTransacao;
import uvafat.faturamento.negocio.api.servico.usuarios.RegistroUsuario;
import uvafat.faturamento.negocio.api.servico.usuarios.RepositorioUsuario;
import uvafat.faturamento.negocio.impl.memoria.infra.detail.DaoGenericoMemoria;

import java.util.Optional;

public class RepositorioUsuarioImpl implements RepositorioUsuario {

    private DaoGenericoMemoria dao;

    public RepositorioUsuarioImpl(DaoGenericoMemoria dao) {
        this.dao = dao;
        this.dao.registrarImplementacao(Usuario.class, DadosUsuario.class);
    }

    @Override
    public RegistroUsuario criar(String nome) {
        DadosUsuario dados = new DadosUsuario(nome);
        return doSalvar(dados);
    }

    @Override
    public Optional<RegistroUsuario> buscar(Usuario entidade) {
        final Usuario entidadePesquisa = entidade instanceof RegistroUsuario ? RegistroUsuario.class.cast(entidade).getDadosEntidade() : entidade;
        Optional<Usuario> entidadeEncontrada = dao.buscar(entidadePesquisa);

        if (!entidadeEncontrada.isPresent()) {
            entidadeEncontrada = dao.buscar(Usuario.class,
                dadosUsuario -> dadosUsuario.getNome().equals(entidade.getNome()));
        }

        if (entidadeEncontrada.isPresent()) {
            DadosUsuario dados = DadosUsuario.class.cast(entidadeEncontrada.get());
            return Optional.of(instanciarRegistro(dados));
        }

        return Optional.empty();
    }

    @Override
    public RegistroUsuario salvar(Usuario entidade) {
        DadosUsuario dados = entidade instanceof DadosUsuario
            ? DadosUsuario.class.cast(entidade)
            : new DadosUsuario(entidade);
        return doSalvar(dados);
    }

    private RegistroUsuario doSalvar(DadosUsuario dados) {
        Optional<Long> novoId = dao.salvar(dados);

        if (novoId.isPresent()) {
            Usuario.Id id = new Usuario.Id(novoId.get());
            dados.setId(id);
        }

        return instanciarRegistro(dados);
    }

    private RegistroUsuario instanciarRegistro(DadosUsuario dados) {
        return new RegistroUsuario(
            dados,
            this,
            Repositorios.para(RepositorioTransacao.class),
            Repositorios.para(RepositorioMeioPagamento.class)
        );
    }
}
