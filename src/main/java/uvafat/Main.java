package uvafat;

import uvafat.faturamento.cli.Cli;
import uvafat.faturamento.cli.provedores.ProvedorServicosCli;
import uvafat.faturamento.cli.provedores.ProvedoresServicoCli;

import java.rmi.RMISecurityManager;

/**
 * Hello world!
 *
 */
public class Main {

    public static void main( String[] args ) {

//        if (System.getSecurityManager() == null) {
//            System.setProperty("java.security.policy", "classpath:uvafat/java.policy");
//            System.setProperty("java.security.debug", "access,failure");
//            SecurityManager sm = new SecurityManager();
//            System.setSecurityManager(sm);
//        }

        System.err.println("* Iniciando aplicação...");
        try(ProvedorServicosCli provedor = ProvedoresServicoCli.instanciar();
            Cli cli = new Cli(provedor, args)
        ) {
            cli.run();
            System.err.println("* Finalizando aplicação...");
        }
        System.err.println("* Aplicação finalizada.");
    }


}
